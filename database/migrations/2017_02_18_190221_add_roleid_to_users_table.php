<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddRoleidToUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
          $table->string('email')->nullable();
          $table->string('profilepic')->nullable();
          $table->string('address')->nullable();
          $table->string('city')->nullable();
          $table->string('district')->nullable();
          $table->string('country')->nullable();
          $table->string('postal_code')->nullable();
          $table->integer('role_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
          $table->dropColumn('email');
          $table->dropColumn('role_id');
        });
    }
}
