<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         $this->call(UsersTableSeeder::class);
         $this->call(PropertyTypeTableSeeder::class);
         $this->call(AdvertisementCredentialsTableSeeder::class);
         $this->call(AvailablitiesTableSeeder::class);
         $this->call(BedroomsTableSeeder::class);
         $this->call(FeaturesTableSeeder::class);
         $this->call(ServicesTableSeeder::class);
         $this->call(RolesTableSeeder::class);
         
    }
}
