<?php

use Illuminate\Database\Seeder;
use App\Bedroom;

class BedroomsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      Bedroom::truncate();

      $bedrooms = [
        ['name'=>'1BHK','rooms'=>'1','key_search'=>''],
        ['name'=>'2BHK','rooms'=>'2','key_search'=>''],
        ['name'=>'3BHK','rooms'=>'3','key_search'=>''],
        ['name'=>'4BHK','rooms'=>'4','key_search'=>''],
        ['name'=>'5BHK','rooms'=>'5','key_search'=>''],
        ['name'=>'6BHK','rooms'=>'6','key_search'=>''],
        ['name'=>'7BHK','rooms'=>'7','key_search'=>''],
        ['name'=>'8BHK','rooms'=>'8','key_search'=>'']
      ];

      foreach ($bedrooms as $bedroom) {
        $newbedroom = new Bedroom;
        $newbedroom->name = $bedroom['name'];
        $newbedroom->rooms = $bedroom['rooms'];
        $newbedroom->key_search = $bedroom['key_search'];
        $newbedroom->save();
        }
    }
}
