<?php

use Illuminate\Database\Seeder;
use App\PropertyType;

class PropertyTypeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        PropertyType::truncate();

        $propertytypes = [
          ['name'=>'Apartment'],
          ['name'=>'Duplex'],
          ['name'=>'Villa'],
          ['name'=>'PG/Hostel'],
          ['name'=>'Office/Workplace']
        ];

        foreach ($propertytypes as $propertytype) {
          $newpropertytype = new PropertyType;
          $newpropertytype->name = $propertytype['name'];
          $newpropertytype->save();
          }
    }
}
