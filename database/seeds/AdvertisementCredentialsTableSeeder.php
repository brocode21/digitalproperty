<?php

use Illuminate\Database\Seeder;
use App\AdvertisementCredential;

class AdvertisementCredentialsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        AdvertisementCredential::truncate();

        $advertisementcredentials = [
          ['postby'=>'Owner','key_search'=>''],
          ['postby'=>'Builder','key_search'=>''],
          ['postby'=>'Dealer','key_search'=>'']
        ];

        foreach ($advertisementcredentials as $advertisementcredential) {
          $newcredential = new AdvertisementCredential;
          $newcredential->postby = $advertisementcredential['postby'];
          $newcredential->key_search = $advertisementcredential['key_search'];
          $newcredential->save();
          }

    }
}
