<?php

use Illuminate\Database\Seeder;
use App\Service;

class ServicesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      Service::truncate();

      $services = [
        ['name'=>'Power Back-up','key_search'=>''],
        ['name'=>'Reserved Parking','key_search'=>''],
        ['name'=>'Lift(s)','key_search'=>''],
        ['name'=>'Security / Fire Alarm','key_search'=>'']

      ];

      foreach ($services as $service) {
        $newservice = new Service;
        $newservice->name = $service['name'];
        $newservice->key_search = $service['key_search'];
        $newservice->save();
        }
    }
}
