<?php

use Illuminate\Database\Seeder;
use App\Availablity;

class AvailablitiesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      Availablity::truncate();

    $availables = [
      ['name'=>'Family','key_search'=>''],
      ['name'=>'Joint Family','key_search'=>''],
      ['name'=>'Single Women','key_search'=>''],
      ['name'=>'Single Men','key_search'=>''],
      ['name'=>'Tenants with Company Lease ','key_search'=>'']
    ];

    foreach ($availables as $available) {
      $newavailable = new Availablity;
      $newavailable->name = $available['name'];
      $newavailable->key_search = $available['key_search'];
      $newavailable->save();
      }
    }
}
