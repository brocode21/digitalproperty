<?php

use Illuminate\Database\Seeder;
use App\Feature;

class FeaturesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        Feature::truncate();

        $features = [
          ['name'=>'Furnished','key_search'=>''],
          ['name'=>'Non Furnished','key_search'=>''],
          ['name'=>'Balcony','key_search'=>''],
          ['name'=>'Servant Room','key_search'=>''],
          ['name'=>'Marbled Floor','key_search'=>'']
        ];

        foreach ($features as $feature) {
          $newfeature = new Feature;
          $newfeature->name = $feature['name'];
          $newfeature->key_search = $feature['key_search'];
          $newfeature->save();
          }
    }
}
