<?php

use Illuminate\Database\Seeder;
use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      User::truncate();

    $users = [
      ['username'=>'SuperAdmin','phone'=>'1234567890','password'=>'123456','role_id'=>'1','email'=>'admin@digitalpropery.com'],
      ['username'=>'Agent','phone'=>'1234567891','password'=>'123456','role_id'=>'2'  ,'email'=>'admin@digitalpropery.com'],
      ['username'=>'Agent1','phone'=>'1234567893','password'=>'123456','role_id'=>'2' ,'email'=>'agent1@gmail.com'],
      ['username'=>'Agent2','phone'=>'1234567894','password'=>'123456','role_id'=>'2' ,'email'=>'agent2@gmail.com'],
      ['username'=>'Agent3','phone'=>'1234567895','password'=>'123456','role_id'=>'2' ,'email'=>'agent3@digitalpropery.com'],
      ['username'=>'Agent4','phone'=>'1234567896','password'=>'123456','role_id'=>'2' ,'email'=>'agent4@digitalpropery.com'],
      ['username'=>'Agent5','phone'=>'1234567897','password'=>'123456','role_id'=>'2' ,'email'=>'agent5@gmail.com'],
      ['username'=>'Agent6','phone'=>'1234567898','password'=>'123456','role_id'=>'2' ,'email'=>'agent6@digitalpropery.com'],
      ['username'=>'Agent7','phone'=>'1234567899','password'=>'123456','role_id'=>'2' ,'email'=>'agent7@digitalpropery.com'],
      ['username'=>'Agent8','phone'=>'1234567800','password'=>'123456','role_id'=>'2' ,'email'=>'agent8@gmail.com'],
      ['username'=>'Agent9','phone'=>'1234567801','password'=>'123456','role_id'=>'2' ,'email'=>'agent9@digitalpropery.com'],
      ['username'=>'Agent10','phone'=>'1234567802','password'=>'123456','role_id'=>'2' ,'email'=>'agent10@gmail.com'],
      ['username'=>'Agent11','phone'=>'1234567803','password'=>'123456','role_id'=>'2' ,'email'=>'agent11@digitalpropery.com'],
      ['username'=>'Agent12','phone'=>'1234567804','password'=>'123456','role_id'=>'2' ,'email'=>'agent12@digitalpropery.com'],
      ['username'=>'User','phone'=>'1234567805','password'=>'123456','role_id'=>'3'   ,'email'=>'user@digitalpropery.com'],
      ['username'=>'User1','phone'=>'1234567806','password'=>'123456','role_id'=>'3' ,'email'=>'user1@gmail.com'],
      ['username'=>'User2','phone'=>'1234567807','password'=>'123456','role_id'=>'3' ,'email'=>'user2@digitalpropery.com'],
      ['username'=>'User3','phone'=>'1234567808','password'=>'123456','role_id'=>'3' ,'email'=>'user3@gmail.com'],
      ['username'=>'User4','phone'=>'1234567809','password'=>'123456','role_id'=>'3' ,'email'=>'user4@digitalpropery.com'],
      ['username'=>'User5','phone'=>'1234567810','password'=>'123456','role_id'=>'3' ,'email'=>'user5@gmail.com'],
      ['username'=>'User6','phone'=>'1234567811','password'=>'123456','role_id'=>'3' ,'email'=>'user6@digitalpropery.com'],
      ['username'=>'User7','phone'=>'1234567812','password'=>'123456','role_id'=>'3' ,'email'=>'user7@digitalpropery.com'],
      ['username'=>'User8','phone'=>'1234567813','password'=>'123456','role_id'=>'3' ,'email'=>'user8@gmail.com'],
      ['username'=>'User9','phone'=>'1234567814','password'=>'123456','role_id'=>'3' ,'email'=>'user9@digitalpropery.com'],
      ['username'=>'User10','phone'=>'1234567883','password'=>'123456','role_id'=>'3' ,'email'=>'user10@gmail.com']
    ];

    foreach ($users as $key => $user) {
      $newuser = new User;
      $newuser->username = $user['username'];
      $newuser->phone = $user['phone'];
      $newuser->role_id = $user['role_id'];
      $newuser->email = $user['email'];
      $newuser->password = \Hash::make($user['password']);
      $newuser->save();
      }
    }
}
