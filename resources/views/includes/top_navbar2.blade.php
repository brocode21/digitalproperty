
<div class="row border-bottom white-bg">
<nav class="navbar navbar-static-top" role="navigation">
    <div class="navbar-collapse collapse" id="navbar">
        <!--ul class="nav navbar-nav">
            <li class="dropdown">
                <a aria-expanded="false" role="button" href="#" class="dropdown-toggle" data-toggle="dropdown"> Menu item <span class="caret"></span></a>
                <ul role="menu" class="dropdown-menu">
                    <li><a href="">Menu item</a></li>
                    <li><a href="">Menu item</a></li>
                    <li><a href="">Menu item</a></li>
                    <li><a href="">Menu item</a></li>
                </ul>
            </li>
        </ul-->
        <ul class="nav navbar-top-links navbar-right">
          @if(Auth::check())
          <li class="active">
              <a aria-expanded="false" role="button" href="/property/allproperty">
                  <strong><i class="fa fa-search"></i>Search Propertie(s) </strong>
              </a>
          </li>
          <li class="active">
              <a aria-expanded="false" role="button" href="/manage/property/view">
                  <strong>Manage Properties </strong>
              </a>
          </li>
          <li class="dropdown active">
              <a aria-expanded="false" role="button" href="#" class="dropdown-toggle" data-toggle="dropdown"> Menu item <span class="caret"></span></a>
              <ul role="menu" class="dropdown-menu">
                  <li><a href="">Menu item</a></li>
                  <li><a href="">Menu item</a></li>
                  <li><a href="">Menu item</a></li>
                  <li><a href="">Menu item</a></li>
              </ul>
          </li>
          @else
          <li>
          </li>        
          @endif
        </ul>
    </div>
</nav>
</div>
