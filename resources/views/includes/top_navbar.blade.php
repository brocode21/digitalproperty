
<div class="row border-bottom white-bg">
<nav class="navbar navbar-static-top" role="navigation">
    <div class="navbar-header">
        <button aria-controls="navbar" aria-expanded="false" data-target="#navbar" data-toggle="collapse" class="navbar-toggle collapsed" type="button">
            <i class="fa fa-reorder"></i>
        </button>
        <a href="/" class="navbar-brand">
          <i class="fa fa-home"></i>
          <strong>Digital Property </strong>
        </a>
    </div>
    <div class="navbar-collapse collapse" id="navbar">
        <!--ul class="nav navbar-nav">

            <li class="dropdown">
                <a aria-expanded="false" role="button" href="#" class="dropdown-toggle" data-toggle="dropdown"> Menu item <span class="caret"></span></a>
                <ul role="menu" class="dropdown-menu">
                    <li><a href="">Menu item</a></li>
                    <li><a href="">Menu item</a></li>
                    <li><a href="">Menu item</a></li>
                    <li><a href="">Menu item</a></li>
                </ul>
            </li>
            <li class="dropdown">
                <a aria-expanded="false" role="button" href="#" class="dropdown-toggle" data-toggle="dropdown"> Menu item <span class="caret"></span></a>
                <ul role="menu" class="dropdown-menu">
                    <li><a href="">Menu item</a></li>
                    <li><a href="">Menu item</a></li>
                    <li><a href="">Menu item</a></li>
                    <li><a href="">Menu item</a></li>
                </ul>
            </li>
            <li class="dropdown">
                <a aria-expanded="false" role="button" href="#" class="dropdown-toggle" data-toggle="dropdown"> Menu item <span class="caret"></span></a>
                <ul role="menu" class="dropdown-menu">
                    <li><a href="">Menu item</a></li>
                    <li><a href="">Menu item</a></li>
                    <li><a href="">Menu item</a></li>
                    <li><a href="">Menu item</a></li>
                </ul>
            </li>
            <li class="dropdown">
                <a aria-expanded="false" role="button" href="#" class="dropdown-toggle" data-toggle="dropdown"> Menu item <span class="caret"></span></a>
                <ul role="menu" class="dropdown-menu">
                    <li><a href="">Menu item</a></li>
                    <li><a href="">Menu item</a></li>
                    <li><a href="">Menu item</a></li>
                    <li><a href="">Menu item</a></li>
                </ul>
            </li>

        </ul -->
        <ul class="nav navbar-top-links navbar-right">
          @if(Auth::check())
          <li class="active">
              <a aria-expanded="false" role="button" href="/allproperty">
                  <strong><i class="fa fa-search"></i>Search Propertie(s) </strong>
              </a>
          </li>
          <li class="dropdown active">
              <a aria-expanded="false" role="button" href="#" class="dropdown-toggle" data-toggle="dropdown">
                <strong>Manage Digitalproperty </strong>
                <span class="caret"></span></a>
              <ul role="menu" class="dropdown-menu">
                  <li><a href="/manage/user">Manage Users</a></li>
                  <li><a href="/manage/chats">Manage Chats</a></li>
                  <li class=""><a href="/manage/property/view">
                          <strong>Manage Properties </strong>
                      </a>
                  </li>
              </ul>
          </li>

          <li>
            <a href="{{ url('/logout') }}"
                onclick="event.preventDefault();
                         document.getElementById('logout-form').submit();">
                <i class="fa fa-sign-out"></i> Log Out
            </a>
            <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                {{ csrf_field() }}
            </form>
          </li>
          <li>
              <a class="right-sidebar-toggle">
                  <i class="fa fa-tasks"></i>
              </a>
          </li>
          @else
          <li class="">
              <a href="/allproperty">
                  <strong>View all Properties </strong>
              </a>
          </li>
          <li>
            <a href="{{ url('/login') }}" >
                <i class="fa fa-sign-in"></i> Log in
            </a>
          </li>
          @endif
        </ul>
    </div>
</nav>
</div>
