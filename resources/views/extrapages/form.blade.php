<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>FooTable</h2>
        <ol class="breadcrumb">
            <li>
                <a href="index.html">Home</a>
            </li>
            <li>
                <a>Tables</a>
            </li>
            <li class="active">
                <strong>FooTable</strong>
            </li>
        </ol>
    </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
<div class="row">
           <div class="col-lg-12">
               <div class="ibox float-e-margins">
                   <div class="ibox-title">
                       <h5>All form elements <small>With custom checbox and radion elements.</small></h5>
                       <div class="ibox-tools">
                           <a class="collapse-link">
                               <i class="fa fa-chevron-up"></i>
                           </a>
                           <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                               <i class="fa fa-wrench"></i>
                           </a>
                           <ul class="dropdown-menu dropdown-user">
                               <li><a href="#">Config option 1</a>
                               </li>
                               <li><a href="#">Config option 2</a>
                               </li>
                           </ul>
                           <a class="close-link">
                               <i class="fa fa-times"></i>
                           </a>
                       </div>
                   </div>
                   <div class="ibox-content">
                       <form method="get" class="form-horizontal">
                           <div class="form-group"><label class="col-sm-2 control-label">Normal</label>

                               <div class="col-sm-10"><input type="text" class="form-control"></div>
                           </div>
                           <div class="hr-line-dashed"></div>
                           <div class="form-group"><label class="col-sm-2 control-label">Help text</label>
                               <div class="col-sm-10"><input type="text" class="form-control"> <span class="help-block m-b-none">A block of help text that breaks onto a new line and may extend beyond one line.</span>
                               </div>
                           </div>
                           <div class="hr-line-dashed"></div>
                           <div class="form-group"><label class="col-sm-2 control-label">Password</label>

                               <div class="col-sm-10"><input type="password" class="form-control" name="password"></div>
                           </div>
                           <div class="hr-line-dashed"></div>
                           <div class="form-group"><label class="col-sm-2 control-label">Placeholder</label>

                               <div class="col-sm-10"><input type="text" placeholder="placeholder" class="form-control"></div>
                           </div>
                           <div class="hr-line-dashed"></div>
                           <div class="form-group">
                               <div class="col-sm-4 col-sm-offset-2">
                                   <button class="btn btn-white" type="submit">Cancel</button>
                                   <button class="btn btn-primary" type="submit">Save changes</button>
                               </div>
                           </div>
                       </form>
                   </div>
               </div>
           </div>
       </div>
</div>
