@extends('layouts.default')
@section('content')


<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-12">
        <h2>View Your Users profile</h2>
        <a href="/manage/user" class="btn btn-white pull-right btn-sm" data-toggle="tooltip" title="Digitalproperty users list" style="margin-top: -40px;">
          <i class="fa fa-arrow-circle-left"></i> Back to homepage
        </a>
    </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
              <div class="row animated fadeInRight">
                  <div class="col-md-4">
                      <div class="ibox float-e-margins">
                          <div class="ibox-title">
                              <h5>Profile Detail</h5>
                          </div>
                          <div>
                              <div class="ibox-content no-padding border-left-right">
                                  <img alt="image" class="img-responsive" src="@if($user->profilepic) {{$user->profilepic}} @else /assets/img/nouserimage.jpg @endif">
                              </div>
                              <div class="ibox-content profile-content">
                                  <h4><strong>{{$user->username}}</strong></h4>
                                  <p><i class="fa fa-map-marker"></i>
                                    @if(($user->address))
                                     {{$user->address}}
                                     @else
                                     Address has not been mentioned yet...
                                     @endif
                                   </p>
                                  <h5>
                                      About me
                                  </h5>
                                  <p>
                                    @if(($user->aboutme))
                                     {{$user->aboutme}}
                                     @else
                                     About me has not been mentioned yet...
                                     @endif                                  </p>
                                  <div class="row m-t-lg">
                                      <div class="col-md-4">
                                          <span class="bar">5,3,9,6,5,9,7,3,5,2</span>
                                          <h5><strong>{{$user->propertyCount->count()}}</strong> Propertie(s)</h5>
                                      </div>
                                      <div class="col-md-4">
                                          <span class="line">5,3,9,6,5,9,7,3,5,2</span>
                                          <h5><strong></strong> Wishlisht</h5>
                                      </div>
                                      <div class="col-md-4">
                                          <span class="bar">5,3,2,-1,-3,-2,2,3,5,2</span>
                                          <h5><strong></strong> Cart</h5>
                                      </div>
                                  </div>
                                  <div class="user-button">
                                      <div class="row">
                                          <div class="col-md-6">
                                              <button type="button" class="btn btn-primary btn-sm btn-block"><i class="fa fa-check-circle"></i> Profile Completion</button>
                                          </div>
                                          <div class="col-md-6">
                                              <button type="button" class="btn btn-default btn-sm btn-block"><i class="fa fa-envelope"></i> Request for verification</button>
                                          </div>
                                      </div>
                                  </div>
                              </div>
                      </div>
                    </div>
                  </div>

                  <div class="col-md-8">
                      <div class="ibox float-e-margins">
                          <div class="ibox-title">
                              <h5>{{$user->username}} Activites</h5>
                              <div class="ibox-tools">
                              </div>
                          </div>
                          <div class="ibox-content" style="max-height:20px,overflow-y:auto;">
                              <div>
                                  <div class="feed-activity-list">
                                    @foreach($user->propertyCount as $property)
                                      <div class="feed-element">
                                          <a href="#" class="pull-left">
                                              <img alt="image" class="img-circle" src="/assets/img/a3.jpg">
                                          </a>
                                            <small class="pull-right">{{$property->created_at}}</small>
                                          <div class="media-body row ">
                                              <div class="col-md-6" style="max-width:350px;overflow-x:auto;">
                                                <strong>{{$user->username}}</strong> added <strong>{{$property->name}}</strong>property. <br>
                                                <hr>
                                                <div>
                                                    <h3 class="product-main-price">Rs {{$property->rent}} /- <small class="text-muted">Exclude Tax</small> </h3>
                                                    <span class="pull-right ">
                                                       <small><strong>{{$property->bedroom->name}}/{{$property->bedroom->rooms}} Bedroom(s)</strong></small>
                                                    </span>
                                                </div>
                                                  <div class="row">
                                                    <div class="col-md-4">
                                                      <ul class="stat-list m-t-lg">
                                                        <li><strong>Available For:</strong>
                                                          <ul>
                                                            @foreach($property->availablity as $availablity)
                                                              <li class="no-margins ">{{$availablity->availablityname->name}}</li>
                                                            @endforeach
                                                          </ul>
                                                        </li>
                                                      </ul>
                                                    </div>
                                                    <div class="col-md-4">
                                                      <ul class="stat-list m-t-lg">
                                                        <li><strong>Services:</strong>
                                                          <ul>
                                                            @foreach($property->service as $service)
                                                              <li class="no-margins ">{{$service->servicename->name}}</li>
                                                            @endforeach
                                                          </ul>
                                                        </li>
                                                      </ul>
                                                    </div>
                                                    <div class="col-md-4">
                                                      <ul class="stat-list m-t-lg">
                                                        <li><strong>Features:</strong>
                                                          <ul>
                                                            @foreach($property->feature as $feature)
                                                              <li class="no-margins ">{{$feature->featurename->name}}</li>
                                                            @endforeach
                                                          </ul>
                                                        </li>
                                                      </ul>
                                                    </div>
                                                  </div>
                                                  <div class="col-md-12" style="margin-top:20px">
                                                     <strong>Property Address:</strong>{{$property->formattedaddress}}
                                                </div>
                                              </div>

                                                <div class="col-md-6 col-md-offset-1">
                                                  @if($property->propertyImage->count())
                                                  <strong>{{$user->username}}</strong> added {{$property->propertyImage->count()}} photo(s) on <strong>Digital Property</strong>. <br>
                                                  <div class="photos">
                                                    @foreach($property->propertyImage as $key=>$image)
                                                      <a target="_blank" href="{{$property->propertyImage->take(1)}}</small>" @if($key != 0 ) style="display:none" @endif>
                                                        <img alt="image" class="feed-photo" src="{{$image->name}}">
                                                      </a>
                                                    @endforeach
                                                    <br>
                                                    @if($property->propertyImage)
                                                    <a href="/manage/user/{{$user->id}}/viewprofile/{{$property->id}}/viewallpropertyimages" class="text-muted text-right">See All..</a>
                                                    @endif
                                                  </div>
                                                  <div class="actions">
                                                      <!--a class="btn btn-xs btn-white"><i class="fa fa-thumbs-up"></i> Like </a-->
                                                      <a class="btn btn-xs btn-danger"><i class="fa fa-heart"></i> Wishlist</a>
                                                      <a class="btn btn-xs btn-white"><i class="fa fa-heart"></i> Wishlist</a>
                                                      <!--a class="btn btn-xs btn-primary"><i class="fa fa-pencil"></i> Message</a-->
                                                  </div>
                                                  @else
                                                  <div class="photos">
                                                    <img alt="image" class="feed-photo" src="/assets/img/image-not-found.jpg">
                                                  </div>
                                                  <p>
                                                    No images has yet been uploaded for this property.                                                    <br>
                                                    <a href="">Request for image(s)...</a>
                                                  </p>
                                                  @endif
                                                  </div>
                                          </div>
                                      </div>
                                    @endforeach
                                  </div>
                              </div>
                          </div>
                      </div>
                  </div>
              </div>

</div>
@endsection
@section('extra_script')

@endsection
