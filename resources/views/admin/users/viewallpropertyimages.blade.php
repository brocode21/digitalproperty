@extends('frontend.index')
@section('content')
   <div class="row wrapper border-bottom white-bg page-heading">
            <div class="col-lg-12">
                <h2>Rental Property detail (Images)
                </h2>
                <a id="decoder" href="/manage/user/{{$userid}}/viewprofile" class="btn btn-white pull-right" data-toggle="tooltip" title="Go back to Properties " style="margin-top: -40px;">
                  <i class="fa fa-arrow-circle-left"></i> Go Back
                </a>
            </div>
  </div>
  <div class="wrapper wrapper-content animated fadeInRight" >
    <div class="row" >
      @include('admin.property.viewproperty.propertylightbox')
    </div>
  </div>

@endsection
