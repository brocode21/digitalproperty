@extends('layouts.default')
@section('content')


<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-12">
        <h2>View Your Users</h2>
        <a href="/" class="btn btn-white pull-right btn-sm" data-toggle="tooltip" title="Digitalproperty home" style="margin-top: -40px;">
          <i class="fa fa-arrow-circle-left"></i> Back to homepage
        </a>
    </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
      <div class="col-lg-12">
                        <div class="ibox float-e-margins">
                            <div class="ibox-title">
                                <h5>Custom responsive Users table </h5>
                                <div class="ibox-tools">
                                </div>
                            </div>
                            <div class="ibox-content">
                                <div class="row">
                                    <div class="col-sm-9 m-b-xs">
                                        <div data-toggle="buttons" class="btn-group">
                                          <label class="btn btn-sm btn-white active"> <input type="radio" id="option2" name="options"> All Roles(s) </label>
                                          @foreach(App\Role::all() as $role)
                                            <label class="btn btn-sm btn-white filteruser" data-id = "{{$role->id}}" >{{$role->name}} </label>
                                          @endforeach
                                        </div>
                                    </div>
                                    <form method="get" action="/manage/user">
                                      <div class="col-sm-3">
                                          <div class="input-group"><input type="text" placeholder="Search" value = "@if($searchUser) {{$searchUser}} @endif"name="searchUser" class="input-sm form-control">
                                            <span class="input-group-btn">
                                              <button type="submit" class="btn btn-sm btn-primary"> Go!</button>
                                            </span>
                                          </div>
                                      </div>
                                    </form>
                                </div>
                                <div class="table-responsive project-list">
                                    <table class="table table-striped table table-hover">
                                        <thead>
                                        <tr>

                                            <th>#</th>
                                            <th>Name </th>
                                            <th>Verified </th>
                                            <th>Phone </th>
                                            <th>Role </th>
                                            <th>Status /<br>Last login </th>
                                            <th>Profile <br>completed</th>
                                            <th>Created at</th>
                                            <th>Action</th>
                                            <th>Propertie(s) </th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                          @foreach($users as $key => $user)
                                        <tr>
                                            <td>{{++$key}}</td>
                                            <td>
                                              {{$user->username}} </td>
                                            <td>
                                              <a class="project-people" href="">
                                                <img alt="image" class="img-circle" src="/assets/img/a3.jpg" data-toggle="tooltip" title="Digitalproperty home" style="margin-left:15px;margin-right:15px;"> </a>
                                                @if($user->email)
                                                <i class="fa fa-check text-navy" data-toggle="tooltip" title="Verified by email"></i>
                                                @else
                                                <i class="fa fa-times text-navy" style="color:red;" data-toggle="tooltip" title="Not verified by phone"></i>
                                                @endif
                                              </td>
                                            <td>{{$user->phone}}</td>
                                            <td>{{$user->userRole->name}}</td>
                                            <td>
                                              @if($user->status == 1)
                                                <span class="" style="margin-left:15px;margin-right:15px;color:green;">
                                                  <i class="fa fa-toggle-on" aria-hidden="true"></i>
                                                </span>
                                              @else
                                                <span class="" style="margin-left:15px;margin-right:15px;">
                                                  <i class="fa fa-toggle-off" aria-hidden="true"></i>
                                                  <br>
                                                  {{$user->updated_at->diffforhumans()}}
                                                </span>
                                              @endif
                                            </td>
                                            <td>100%</td>
                                            <td>{{$user->created_at->diffforhumans()}}</td>
                                            <td>
                                                <!--a href="/manage/user/{{$user->id}}/editprofile"class="tips btn btn-xs btn-white" data-toggle="tooltip" title="Edit Profile">
                                                <i class="fa fa-edit"></i>
                                              </a-->
                                                <a href="/manage/user/{{$user->id}}/viewprofile"class="tips btn btn-xs btn-white" data-toggle="tooltip" title="View Profile">
                                                <i class="fa fa-eye"></i>
                                                </a>
                                                <a href="/manage/user/{{$user->id}}/delete"class="tips btn btn-xs btn-white" data-toggle="tooltip" title="Delete ">
                                                <i class="fa fa-trash-o"></i>
                                                </a>
                                                @if(Auth::user()->role_id == '1' & $user->role_id != '1')
                                                <a href="/manage/user/{{$user->id}}/accessprofile"class="tips btn btn-xs btn-white" data-toggle="tooltip" title="Access Profile">
                                                <i class="fa fa-key"></i>
                                                </a>
                                                @endif
                                            </td>
                                            <td>{{$user->propertyCount->count()}}</td>
                                        </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>

                            </div>
                        </div>
                    </div>
    </div>
</div>
@endsection
@section('extra_script')

@endsection
