@extends('frontend.index')
@section('content')
<div class="row">
    <div class="col-lg-12">
      <form method="post"  class=""  action="/manage/property/imagestore" enctype="multipart/form-data">

    <div class="ibox float-e-margins">
        <div class="ibox-title">
            <h5>Dropzone Area for Property Image(s)*</h5>
        </div>
        <div class="ibox-content">
            {{ csrf_field()}}
             <input type="hidden" name="property_id" value="{{$property_id}}">
             <input type="file" name="property_image[]" value="" multiple='multiple' required>
              <div>
                  <div class="m text-left">
                    <small>
                      <a href="/manage/property/view" class="btn btn-primary">
                        Skip >>
                      </a>
                      <button class="btn btn-success pull-right" type="submit">
                          Add Images
                        </button>
                    </small>
                  </div>
              </div>
        </div>
    </div>
</div>
</form>
</div>
@endsection
