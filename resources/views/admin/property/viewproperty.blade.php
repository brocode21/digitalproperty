@extends('frontend.index')
@section('content')
   <div class="row wrapper border-bottom white-bg page-heading">
            <div class="col-lg-12">
                <h2>Rental Property detail</h2>
                <a id="decoder" href="{{URL::previous()}}" class="btn btn-white pull-right" data-toggle="tooltip" title="Go back to Previous Page " style="margin-top: -40px;">
                  <i class="fa fa-arrow-circle-left"></i> Go Back
                </a>
            </div>
  </div>
  <div class="wrapper wrapper-content animated fadeInRight" >
    <div class="row" >
      <div class="col-lg-12">
        <div class="ibox product-detail">
            <div class="ibox-content">
                <div class="row">
                    <div class="col-md-6">
                      <div class="ibox-title">
                        <h2 class="font-bold m-b-xs">
                            {{$property->name}}
                        </h2>
                      </div>
                      <div class="ibox-content">
                        @if($property->lat && $property->long)
                        <h4>
                          <i class="fa fa-map-marker" aria-hidden="true"></i>
                          Locate Your Neighbours.</h4>
                        <hr>
                          <p>
                              A place where you can easily locate your property.
                          </p>
                          <div class="google-map" id="map2" style="max-height:500px;height:500px;"></div>
                        @else
                        <div>
                        <img src="/assets/img/nolocation.jpg" style="max-width:600px;width:100%;max-height:400px;height:100%;">
                        <br>
                        <strong><i class="fa fa-map-marker" aria-hidden="true"></i>
                          GoogleMaps location hasn't been uploaded yet for this property.
                          <a class="text-center">Request for exact location..</a>
                        </strong>
                        </div>
                        @endif
                      </div>
                      </div>
                    <div class="col-md-6">
                        <span class="pull-right ">
                           <small><strong>{{$property->bedroom->name}}/{{$property->bedroom->rooms}} Bedroom(s)</strong></small>
                        </span>
                        <strong>Description:</strong><br>{{$property->description}}
                        <hr>
                        <div>
                            <!--button class="btn btn-primary pull-right">Add to cart</button -->
                          <h1 class="product-main-price">Rs {{$property->rent}} /- <small class="text-muted">Exclude Tax</small> </h1>
                          <div class="text-right">
                            <div class="btn-group">
                              @if(isset($property->wishlist))
                               <a href="/manage/property/removewishlist/{{$property->id}}" class="tips btn btn-xs btn-white" data-toggle="tooltip" title="Remove from wshlisht" style="color:gold; font-size: 20px;"><i class="fa fa-star"></i></a>

                               @else
                               <a href="/manage/property/wishlist/{{$property->id}}" class="tips btn btn-xs btn-white" data-toggle="tooltip" title="Add to wishlisht" style="font-size: 20px;"><i class="fa fa-star-o"></i></a>
                              @endif
                          </div>
                        </div>
                      </div>
                        <hr>
                        <dl class="dl-horizontal m-t-md ">
                            <dt>Property posted by</dt>
                            <dd>{{$property->postedBy->userRole->name}}</dd>
                        </dl>
                        <dl class="dl-horizontal m-t-md ">
                            <dt>Property Address</dt>
                            <dd>{{$property->formattedaddress}}</dd>
                            <dt>Zip Code</dt>
                            <dd>{{$property->postal_code}}</dd>
                        </dl>
                        <div class="text-right">
                            <div class="btn-group">
                                <!--button class="btn btn-white btn-sm"><i class="fa fa-star"></i> Add to wishlist </button-->
                                <!--sendmail/contactAgent/{{$property->postedBy->email}}-->
                                <a class="btn btn-white btn-sm" href="" data-toggle="modal" data-target="#myModal2"><i class="fa fa-envelope"></i> Contact with Agent </a>
                            </div>
                        </div>

                          <div class="col-md-4">
                            <ul class="stat-list m-t-lg">
                              <li><strong>Available For:</strong>
                                <ul>
                                  @foreach($property->availablity as $availablity)
                                    <li class="no-margins ">{{$availablity->availablityname->name}}</li>
                                  @endforeach
                                </ul>
                              </li>
                            </ul>
                          </div>
                          <div class="col-md-4">
                            <ul class="stat-list m-t-lg">
                              <li><strong>Services:</strong>
                                <ul>
                                  @foreach($property->service as $service)
                                    <li class="no-margins ">{{$service->servicename->name}}</li>
                                  @endforeach
                                </ul>
                              </li>
                            </ul>
                          </div>
                          <div class="col-md-4">
                            <ul class="stat-list m-t-lg">
                              <li><strong>Features:</strong>
                                <ul>
                                  @foreach($property->feature as $feature)
                                    <li class="no-margins ">{{$feature->featurename->name}}</li>
                                  @endforeach
                                </ul>
                              </li>
                            </ul>
                          </div>
                        <div class="col-md-12" style="margin-top:20px">
                      </div>
                    </div>
                </div>
            </div>
            <div class="ibox-footer">
                <span class="pull-right">
                    Full stock - <i class="fa fa-clock-o"></i> {{$property->created_at}}
                </span>
                {{$property->address}}
            </div>
         </div>
      </div>
      <div class="modal inmodal" id="myModal2" tabindex="-1" role="dialog" aria-hidden="true">
          <div class="modal-dialog">
              <div class="modal-content animated flipInY">
                  <div class="modal-body">
                      <button type="button" class="close" data-dismiss="modal" style="margin: -23px -23px 0px;"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                      <form class="form-horizontal">
                        {{ csrf_field() }}
                          <div class="form-group">
                            <label class="col-lg-2 control-label">Email</label>
                            <div class="col-lg-10"><input type="email" placeholder="Email" class="form-control"> </div>
                          </div>
                          <div class="form-group">
                              <div class="col-lg-offset-2 col-lg-10">
                                  <button class="btn btn-sm btn-primary pull-right" type="submit">Add</button>
                              </div>
                          </div>
                      </form>
                  </div>
              </div>
          </div>
      </div>
      <!--div class="col-md-12">
         <div class="ibox ">
              <div class="ibox-title">
                  <h5>Locate Your Neighbours.</h5>
              </div>
              <div class="ibox-content">
                  <p>
                      A place where you can easily locate your property.
                  </p>
                  <div class="google-map" id="map2"></div>
              </div>
          </div>
      </div-->
      @include('admin.property.viewproperty.propertylightbox')
  </div>

@endsection

@section('extra_script')
<script>
var latitude = '{{$property->lat}}';
var longitude = '{{$property->long}}';
</script>


@endsection
