@extends('layouts.default')
@section('content')


@if($properties->count() > 0)
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-12">
        <h2>View Your Properties</h2>
        <a href="/manage/property/create" class="btn btn-white pull-right btn-sm" data-toggle="tooltip" title="Add property" style="margin-top: -40px;">
          <i class="fa fa-plus"></i> Add New Property
        </a>
    </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                      <h5>Manage Properties &nbsp </h5> {<i> Total Properties - {{$properties->count()}} </i>}
                    <div class="ibox-tools">

                    </div>
                </div>
                <div class="ibox-content">
                    <table class=" table table-stripped toggle-arrow-tiny" data-page-size="8">
                        <thead>
                        <tr>
                            <th >Property Image</th>
                            <th>Name</th>
                            <th>Address</th>
                            <th >Rent</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach ($properties as $key => $property)

                          <tr>
                            <td style="width:200px;height:100px">
                              <div class="carousel slide propertySlider" id="carousel{{$key}}">
                                @if($property->propertyImage->count())
                                    <div class="carousel-inner">
                                        @foreach($property->propertyImage as $k => $propertyimage)
                                        <div class="item @if($k==0) active @endif">
                                            <img alt="image" class="img-responsive" src="{{$propertyimage->name}}" style="width:200px;height:150px;">
                                        </div>
                                        @endforeach
                                    </div>
                                    <a data-slide="prev" href="#carousel{{$key}}" class="left carousel-control">
                                        <span class="icon-prev"></span>
                                    </a>
                                    <a data-slide="next" href="#carousel{{$key}}" class="right carousel-control">
                                        <span class="icon-next"></span>
                                    </a>
                                @else
                                <div class="photos">
                                  <img alt="image" class="feed-photo" src="/assets/img/image-not-found.jpg" style="width:200px;height:150px;">
                                </div>
                                @endif
                              </div>
                              <!--div class="item  active" style="color:red;">
                                    <img alt="Image not Found" class="img-responsive" src="{{$property->property_image}}" style="width:200px;height:100px;">
                                </div-->
                            </td>
                              <td>
                                <strong>{{$property->name}} </strong>
                              </td>
                              <td>{{$property->formattedaddress}}</td>
                              <td>Rs {{$property->rent}} /- </td>
                              <td>
                                <a href="/manage/property/{{$property->id}}/editproperty"class="tips btn btn-xs btn-white" data-toggle="tooltip" title="Edit property">
                                  <i class="fa fa-edit"></i>
                                </a>
                                <a href="/manage/property/{{$property->id}}/viewproperty"class="tips btn btn-xs btn-white" data-toggle="tooltip" title="View property">
                                  <i class="fa fa-eye"></i>
                                </a>
                                <a href="/manage/property/{{$property->id}}/delete"class="tips btn btn-xs btn-white" data-toggle="tooltip" title="Delete ">
                                  <i class="fa fa-trash-o"></i>
                                </a>
                              </td>
                          </tr>
                        @endforeach
                        </tbody>
                        <tfoot>
                        <tr>
                            <td colspan="5">
                                <ul class="pagination pull-right"></ul>
                            </td>
                        </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>


</div>
@else
<div class="wrapper wrapper-content">
    <div class="middle-box text-center animated fadeInRightBig">
        <h3 class="font-bold"></h3>
        <div class="error-desc">
          <p>
            “Home's where you go when you run out of homes.”<br>
            ― <strong>John le Carré, The Honourable Schoolboy </strong>
          </p>
            <br/>
            <a href="/manage/property/create" class="btn btn-primary m-t" data-toggle="tooltip" title="Add property">
              <i class="fa fa-plus"></i> Add Your First Property
            </a>
        </div>
    </div>
</div>
@endif

@endsection

@section('extra_script')
<script>

</script>
@endsection
