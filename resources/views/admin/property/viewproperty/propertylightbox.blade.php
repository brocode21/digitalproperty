
                <div class="col-lg-12">
                <div class="ibox float-e-margins">

                    <div class="ibox-content row">
                      @if($property->propertyImage->count())
                         <div class="col-md-8 col-md-offset-2">
                             <div class="product-images">
                               @foreach($property->propertyImage as $k => $propertyimage)
                                 <div>
                                     <div class="image-imitation" style="padding:0px;max-height: 500px;">
                                       <img src="{{$propertyimage->name}}" style="width:100%;;height: 100%;">
                                     </div>
                                 </div>
                                 @endforeach
                             </div>
                           </div>
                           @endif

                           <!--div class="col-md-3 row">

                             <div class="col-md-12  ">
                               s a touch-enabled, responsive and customizable image & video gallery, carousel and lightbox, optimized for both mobile and desktop web browsers.
                               It features swipe, mouse and keyboard navigation, transition effects, slideshow functionality, fullscreen support and on-demand content loading and can be extended to display additional content types.
                               Full documentation you can find at:
                             </div>
                             <div class="col-md-12">
                               s a touch-enabled, responsive and customizable image & video gallery, carousel and lightbox, optimized for both mobile and desktop web browsers.
                               It features swipe, mouse and keyboard navigation, transition effects, slideshow functionality, fullscreen support and on-demand content loading and can be extended to display additional content types.
                               Full documentation you can find at:
                             </div>
                             s a touch-enabled, responsive and customizable image & video gallery, carousel and lightbox, optimized for both mobile and desktop web browsers.
                             It features swipe, mouse and keyboard navigation, transition effects, slideshow functionality, fullscreen support and on-demand content loading and can be extended to display additional content types.
                             Full documentation you can find at:
                           </div-->

                        <div class="col-md-12">

                        <h2>Lightbox Property image(s) gallery</h2>
                        @if(!$property->propertyImage->count())
                        <p>
                          <strong>
                            Sorry, but no images are available for this property yet.
                          </strong>
                          <a href="">Request for image(s)...</a>
                        </p>
                        @endif
                        <p>
                            <strong>Property Description : </strong> {{$property->description}}
                            <a href="{{Request::fullurl()}}" target="_blank">http://digitalproperty/property/{{$property->id}}/viewproperty</a>
                        </p>
                        <p>
                          <strong>Property Address:</strong>{{$property->formattedaddress}}
                        </p>
                      </div>

                        <div class="lightBoxGallery col-md-12">
                          @foreach($property->propertyImage as $k => $propertyimage)
                            <a href="/property/{{$property->id}}/viewproperty" title="Image from Unsplash" data-gallery=""><img src="{{$propertyimage->name}}" style="width:100%;;height: 100%;max-height: 300px;max-width: 300px;"></a>
                          @endforeach


                            <!-- The Gallery as lightbox dialog, should be a child element of the document body -->
                            <div id="blueimp-gallery" class="blueimp-gallery">
                                <div class="slides"></div>
                                <h3 class="title"></h3>
                                <a class="prev">‹</a>
                                <a class="next">›</a>
                                <a class="close">×</a>
                                <a class="play-pause"></a>
                                <ol class="indicator"></ol>
                            </div>

                        </div>

                    </div>
                </div>
            </div>
