@extends('frontend.index')
@section('content')

<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-12">
        <h2>Add Your Property</h2>
        <div class="sk-spinner sk-spinner-cube-grid pull-left" style="margin-left:70px;">
             <div class="sk-cube"></div>
             <div class="sk-cube"></div>
             <div class="sk-cube"></div>
             <div class="sk-cube"></div>
             <div class="sk-cube"></div>
             <div class="sk-cube"></div>
             <div class="sk-cube"></div>
             <div class="sk-cube"></div>
             <div class="sk-cube"></div>
         </div>           
      <a href="/manage/property/view" class="btn btn-white pull-right" data-toggle="tooltip" title="Go back to Properties " style="margin-top: -40px;">
        <i class="fa fa-arrow-circle-left"></i> Go Back
      </a>
    </div>
</div>
<br>
<div class="wrapper wrapper-content animated fadeInRight">

  <form method="post" class="form-horizontal "  action="/manage/property/store" enctype="multipart/form-data">
    {{ csrf_field()}}
  <div class="row" style="margin-left:20px; margin-right:20px;">
             <div class="col-lg-6"  >
                 <div class="ibox float-e-margins">
                     <div class="ibox-title">
                         <h5>Add Your Property <small></small></h5>                          
                     </div>
                     <div class="ibox-content">
                           <div class="form-group">
                               <div class="col-sm-10">
                                 <input type="hidden" class="form-control" name="agent_id" value="{{Auth::user()->id}}" required>
                               </div>
                           </div>
                             <div class="form-group">
                               <label class="col-sm-2 control-label">Title*</label>
                                 <div class="col-sm-10">
                                   <input type="text" class="form-control" name="name" value="" required>
                                 </div>
                             </div>
                              <div class="hr-line-dashed"></div>
                             <div class="form-group">
                               <label class="col-lg-2 control-label">Address*</label>
                                 <div class="col-lg-10">
                                   <textarea type="text" name="address" class="form-control" value="" required></textarea>
                                   <span class="help-block m-b-none">Manually enter the address <strong>OR</strong> place on map.</span>
                                 </div>
                             </div>
                             <!--div class="hr-line-dashed"></div-->
                             <div class="form-group ">
                               <label class="col-sm-2 control-label">Zip*</label>
                                 <div class="col-sm-4">
                                   <input type="text" name="postal_code" id="postal_code" class="form-control" value=""  >
                                   <span class="help-block m-b-none"></span>
                                 </div>
                             </div>
                             <div class="hr-line-dashed"></div>
                             <div class="form-group">
                               <label class="col-sm-2 control-label">Description</label>
                                 <div class="col-sm-10">
                                   <textarea type="text" name="description" class="form-control" value="" style="height:80px;"></textarea>
                                   <span class="help-block m-b-none"></span>
                                 </div>
                             </div>
                             <div class="hr-line-dashed"></div>
                             <div class="form-group row">
                                 <label class="col-lg-2 control-label">Bedroom(s)* </label>
                                   <div class="col-lg-4">
                                     <select data-placeholder="Choose a Role..." name="bedroom_id" class="chosen-select" style="width:100px;" tabindex="">
                                     @foreach(App\Bedroom::all() as $bedroom)
                                     <option value="{{$bedroom->id}}">{{$bedroom->name}}</option>
                                     @endforeach
                                     </select>
                                   </div>
                              </div>
                              <div class="hr-line-dashed"></div>
                              <div class="form-group">
                                <label class="col-sm-2 control-label">Available for*</label>
                                  <div class="col-sm-10">
                                    <div class="row">
                                      @foreach(App\Availablity::all() as $availablity)
                                        <div class="col-md-6">
                                            <h5 class="no-margins">
                                                <div class="i-checks"><label> <input type="checkbox" name="availablityid[]" value="{{$availablity->id}}"> <i></i> </label> {{$availablity->name}} </div>
                                            </h5>
                                        </div>
                                        @endforeach
                                    </div>
                                  </div>
                              </div>
                              <div class="hr-line-dashed"></div>
                              <div class="form-group">
                                <label class="col-sm-2 control-label">Feature(s)*</label>
                                  <div class="col-sm-10">
                                    <div class="row">
                                      @foreach(App\Feature::all() as $feature)
                                        <div class="col-md-6">
                                            <h5 class="no-margins">
                                                <div class="i-checks"><label> <input type="checkbox" name="featureid[]" value="{{$feature->id}}"> <i></i> </label> {{$feature->name}} </div>
                                            </h5>
                                        </div>
                                        @endforeach
                                    </div>
                                  </div>
                              </div>
                              <div class="hr-line-dashed"></div>
                              <div class="form-group">
                                <label class="col-sm-2 control-label">Service(s)*</label>
                                  <div class="col-sm-10">
                                    <div class="row">
                                      @foreach(App\Service::all() as $service)
                                        <div class="col-md-6">
                                            <h5 class="no-margins">
                                                <div class="i-checks"><label> <input type="checkbox" name="serviceid[]" value="{{$service->id}}"> <i></i> </label> {{$service->name}} </div>
                                            </h5>
                                        </div>
                                        @endforeach
                                    </div>
                                  </div>
                              </div>
                             <div class="hr-line-dashed"></div>
                             <div class="form-group row">
                               <label class="col-sm-2 control-label">Monthly Rent*</label>
                                 <div class="col-sm-4">
                                  <input type="text" name="rent" class="form-control" value="" required>
                                 </div>
                                 <label class="col-lg-2 control-label">Property Type* </label>
                                   <div class="col-lg-4">
                                     <select data-placeholder="Choose a Role..." name="property_type_id" class="chosen-select" style="width:100px;" tabindex="">
                                     @foreach(App\PropertyType::all() as $type)
                                     <option value="{{$type->id}}">{{$type->name}}</option>
                                     @endforeach
                                     </select>
                                   </div>
                              </div>
                             <div class="hr-line-dashed"></div>
                             <div class="form-group row">
                                 <label class="col-lg-2 control-label">You are* </label>
                                   <div class="col-lg-4">
                                     <select data-placeholder="Choose a Role..." name="credential_id" class="chosen-select" style="width:100px;" tabindex="">
                                     @foreach(App\AdvertisementCredential::all() as $credential)
                                     <option value="{{$credential->id}}">{{$credential->postby}}</option>
                                     @endforeach
                                     </select>
                                   </div>
                              </div>
                             <div class="hr-line-dashed"></div>
                             <div class="form-group">
                                 <div class="col-sm-4 col-sm-offset-2">
                                     <button class="btn btn-primary" type="submit">
                                       Add Property
                                     </button>
                                 </div>
                             </div>
                             *fileds are compulsory
                     </div>
                 </div>
             </div>
             <div class="col-lg-6 col">
                 <div class="ibox col-lg-12">
                   <div class="ibox-content">
                          <div id="locationField">
                           <input type="text" id="autocomplete" placeholder="Enter your address" value="" class="form-control" >
                         </div>
                           <span class="help-block m-b-none"></span>
                           <div class="hr-line-dashed"></div>
                         <p>With google maps,You can easily locate your property.</p>
                         <div class="google-map" id="map1" style="max-height:500px;height:500px;"></div>
                         <div id="infowindow-content" style="font-weight: bold;display: none;display: inline;">
                            <img src="" width="16" height="30" id="place-icon" style="display: none;">
                            <span id="place-name"  class="title"></span><br>
                            <span id="place-address"></span>
                         </div>
                           <input type="hidden" id="lat" name="lat" value=" ">
                           <input type="hidden" id="long" name="long" value=" ">
                           <div class="hr-line-dashed"></div>
                           <div class="form-group row">
                           </div>
                           <div class="form-group">
                             <label class="col-sm-2 control-label">Selected Landmark</label>
                               <div class="col-sm-10">
                                 <input type="text" name="formattedaddress"  class="form-control field" value="" readonly >
                                 <span class="help-block m-b-none"></span>
                               </div>
                           </div>

                     </div>
                 </div>
                 <div class="hr-line-dashed"></div>
             </div>
  </div>
  </form>
</div>
@endsection

@section('extra_script')

  
@endsection
