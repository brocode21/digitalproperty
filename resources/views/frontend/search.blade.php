
<div class="property-search-toggle row">
  <form method="post" class="form-horizontal "  action="/property/search" enctype="multipart/form-data">
    {{ csrf_field()}}
    <div class="search-area">
      <div class="col-lg-2 search-components search-box ">
        <div class="" style="margin-bottom:10px;">
             <input type="text" id="searchlocation" name="desiredkeyword" value="" class="form-control"  placeholder="Your Desired Location"  aria-required="true" style="border:none;">
             <input type="hidden" name="lat" value="">
             <input type="hidden" name="long" value="">
             <input type="hidden" name="address" value="">
        </div>
      </div> 
      <div class="col-lg-2 search-components">
        <div class="ibox float-e-margins " style="margin-bottom:10px;">
          <div class="ibox-title left-collapse-link" style="padding-top: 15px;!important;border:none;">
            <h4>Available For &nbsp<i class="fa fa-chevron-down"></i></h4>  
          </div>
          <div class="ibox-content ibox-heading " style="display: none;">
            @foreach(App\Availablity::all() as $availablity)
              <div class="i-checks"><label> <input type="checkbox" name="availablityid[]" value="{{$availablity->id}}"> <i></i> </label> {{$availablity->name}} </div>
            @endforeach 
          </div>
        </div>
      </div>
      <div class="col-lg-2 search-components">
        <div class="ibox float-e-margins " style="margin-bottom:10px;">
          <div class="ibox-title left-collapse-link" style="padding-top: 15px;!important;border:none;">
            <h4>Budget &nbsp<i class="fa fa-chevron-down"></i></h4>  
          </div>
          <div class="ibox-content ibox-heading row" style="display: none;">
            <div class="form-group">            
              <div class="row">
                <div class="col-sm-6">
                  <select name="min_rent" class="change-select form-control">
                      <option value="">Min</option>
                      @foreach($rents as $rent)
                      <option value="{{$rent}}">{{$rent}}</option>
                      @endforeach
                  </select>
                </div>
                <div class="col-sm-6">
                  <select name="max_rent" class="change-select form-control">
                    <option value="">Max</option>
                    @foreach($rents as $rent)
                    <option value="{{$rent}}">{{$rent}}</option>
                    @endforeach
                  </select>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="search-components pull-left" style="margin-bottom:10px; width: 7%;">
        <button  class="btn btn-success search-property-btn" ><i class="fa fa-search"></i> Search </button>
      </div>
    </div>
  </form> 
</div>
