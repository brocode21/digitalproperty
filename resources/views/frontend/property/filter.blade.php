

    <div class="row col-lg-4" >
        <div class="col-md-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <span class="label label-primary pull-right"></span>
                      <h5>Filter <i class="fa fa-filter"></i></h5>
                        <br>
                        @if(Request::segment(1) != 'allproperty')
                        <h6><strong>{{$filteredproperty->count()}} propertie(s) found.</strong></h6>
                        @else
                        <h6><strong>Total properties : {{$filteredproperty->count()}}</strong></h6>
                        @endif
                </div>
            </div>
        </div>
        <div class="col-md-12">
          <div class="ibox float-e-margins">
              <div class="ibox-title">
                  <span class="label label-primary pull-right"></span>
                <h3 class="no-margins">
                  <label>
                  Sort By  <i class="fa fa-sort"></i> : Price </label>
                  <select name="sorting" class="btn btn-success pull-right " style="margin-left:5px;padding:1px;">
                        <option value="1" @if(isset($selectedsorting) && $selectedsorting == '1') selected @endif>Date Added</option>
                        <option value="2" @if(isset($selectedsorting) && $selectedsorting == '2') selected @endif>LOW TO HIGH</option>
                        <option value="3" @if(isset($selectedsorting) && $selectedsorting == '3') selected @endif>HIGH TO LOW</option>
                  </select>
                </h3>
              </div>
          </div>
        </div>

        <div class="col-md-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                        <span class="label label-primary pull-right"></span>
                        <h5>Selected Landmark <i class="fa fa-street-view"></i></h5>
                        <br><h6><strong>  <input type="text" class="form-control field" name="formattedaddress" value=""></strong></h6>
                        <input type="hidden" name="lat" value="">
                        <input type="hidden" name="long" value="">
                        <input type="hidden" name="address" value="">
                </div>
            </div>
        </div>
    @include('frontend.property.filter.availablity')
    @include('frontend.property.filter.rent')
    @include('frontend.property.filter.bedrooms')
    @include('frontend.property.filter.services')
    @include('frontend.property.filter.features')
                  <div class="col-md-12">
                          <div class="ibox float-e-margins">
                            <div class="ibox-title">
                                  <div class="row">
                                      <div class="col-md-12">
                                          <h5 class="no-margins">
                                                <button  class="btn btn-success pull-right " >
                                                  <i class="fa fa-search"></i> Search </button>
                                            </h5>
                                      </div>
                                  </div>
                              </div>
                          </div>
                      </div>
    </div>
