<div class="col-md-12">
  <div class="ibox float-e-margins">
      <div class="ibox-title">
          <span class="label label-primary pull-right"></span>
            <h5>Bedroom(s) <i class="fa fa-bed "></i></h5>
            <div class="ibox-tools">
                  <a class="collapse-link">
                      <i class="fa fa-chevron-up"></i>
                  </a>
            </div>
      </div>
      <div class="ibox-content">
          <div class="row">
              @foreach(App\Bedroom::all() as $bedroom)
              <div class="col-md-4">
                  <h5 class="no-margins">
                      <div class="i-checks col-lg-12"><label> <input type="checkbox" name="bedrooms[]" @if(isset($selectedbedrooms) && !empty($selectedbedrooms) && in_array($bedroom->id, $selectedbedrooms))checked  @endif value="{{$bedroom->id}}"> <i></i> </label> {{$bedroom->name}} </div>
                    </h5>
              </div>
              @endforeach
          </div>
      </div>
  </div>
</div>
