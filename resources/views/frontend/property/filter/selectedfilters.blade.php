<span class=""><strong><i class="fa fa-filter"></i> Filters</strong> : </span>
@if(isset($selectedavailablity) && !empty($selectedavailablity))
@foreach($selectedavailablity as $availablity)
<span class="btn btn-white btn-sm">{{\Helpers::availablityName($availablity)->name}} <i class="fa fa-check-circle"></i></span>
@endforeach
@endif
@if(isset($selectedbedrooms) && !empty($selectedbedrooms))
@foreach($selectedbedrooms as $bedroom)
<span class="btn btn-white btn-sm">{{$bedroom}} Bedrooms(s) <i class="fa fa-bed "></i></span>
@endforeach
@endif
@if(isset($selectedfeatures) && !empty($selectedfeatures))
@foreach($selectedfeatures as $feature)
<span class="btn btn-white btn-sm">{{\Helpers::featureName($feature)->name}} <i class="fa fa-server"></i></span>
@endforeach
@endif
@if(isset($selectedservices)  && !empty($selectedservices))
@foreach($selectedservices as $service)
<span class="btn btn-white btn-sm">{{\Helpers::serviceName($service)->name}} <i class="fa fa-rss-square"></i></span>
@endforeach
@endif
@if(!empty($maxRent))
<br>
<span class=""> <strong><i class="fa fa-money"></i> Max rent </strong>:  {{$maxRent}}</span>
@endif
@if(!empty($minRent))
<br>
<span class=""><strong><i class="fa fa-money"></i> Min rent </strong>: {{$minRent}}</span>
@endif
@if(!empty($selectedkeyword))
<br>
<span class=""><strong><i class="fa fa-keyboard-o"></i> Search Keyword </strong>: {{$selectedkeyword}}</span>
@endif
@if(!empty($selectedaddress))
<br>
<span class=""><strong><i class="fa fa-street-view"></i> Landmark </strong>: {{$selectedaddress}}</span>
@endif
