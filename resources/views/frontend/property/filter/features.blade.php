<div class="col-md-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <span class="label label-primary pull-right"></span>
                  <h5>Feature(s) <i class="fa fa-server"></i></h5>
                  <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                  </div>
            </div>
            <div class="ibox-content">
                <div class="row">
                    @foreach(App\Feature::all() as $feature)
                    <div class="col-md-8">
                        <h5 class="no-margins">
                            <div class="i-checks col-lg-12"><label> <input type="checkbox" name="features[]" @if(isset($selectedfeatures) && !empty($selectedfeatures) && in_array($feature->id, $selectedfeatures))checked  @endif value="{{$feature->id}}"> <i></i> </label> {{$feature->name}} </div>
                          </h5>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
