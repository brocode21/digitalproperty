<div class="col-md-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                  <h5>Rent <i class="fa fa-money"></i></h5>
                  <div class="ibox-tools">
                    <span class="label label-primary">Monthly</span>
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                  </div>
            </div>
            <div class="ibox-content">
                <div class="row">
                    <div class="col-md-6">
                        <h1 class="no-margins">
                          <select name="min_rent" class="change-select form-control">
                                <option value="">Min</option>
                                @foreach($rents as $rent)
                                <option value="{{$rent}}" @if(isset($minRent) && $minRent == $rent) selected @endif>{{$rent}}</option>
                                @endforeach
                            </select>
                          </h1>
                        <div class="font-bold text-navy"> <i class="fa fa-level-up"></i> <small>Select minimum Rent</small></div>
                    </div>
                    <div class="col-md-6">
                        <h1 class="no-margins">
                          <select name="max_rent" class="change-select form-control">
                            <option value="">Max</option>
                            @foreach($rents as $rent)
                            <option value="{{$rent}}" @if(isset($maxRent) && $maxRent == $rent) selected @endif>{{$rent}}</option>
                            @endforeach
                          </select>
                        </h1>
                        <div class="font-bold text-navy"> <i class="fa fa-level-up"></i> <small>Select maximum Rent</small></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
