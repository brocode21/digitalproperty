<div class="col-md-12">
          <div class="ibox float-e-margins">
              <div class="ibox-title">
                  <span class="label label-primary pull-right"></span>
                    <h5>Service(s) <i class="fa fa-rss-square"></i></h5>
                    <div class="ibox-tools">
                          <a class="collapse-link">
                              <i class="fa fa-chevron-up"></i>
                          </a>
                    </div>
              </div>
              <div class="ibox-content">
                  <div class="row">
                      @foreach(App\Service::all() as $service)
                      <div class="col-md-8">
                          <h5 class="no-margins">
                              <div class="i-checks col-lg-12"><label> <input type="checkbox" name="services[]"  @if(isset($selectedservices) && !empty($selectedservices) && in_array($service->id, $selectedservices))checked  @endif value="{{$service->id}}"> <i></i> </label> {{$service->name}} </div>
                            </h5>
                      </div>
                      @endforeach
                  </div>
              </div>
          </div>
      </div>
