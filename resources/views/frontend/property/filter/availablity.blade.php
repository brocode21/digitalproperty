<div class="col-md-12">
    <div class="ibox float-e-margins">
        <div class="ibox-title">
            <span class="label label-primary pull-right"></span>
              <h5>Availabilty <i class="fa fa-check-circle"></i></h5>
              <div class="ibox-tools">
                    <a class="collapse-link">
                        <i class="fa fa-chevron-up"></i>
                    </a>
              </div>
        </div>
        <div class="ibox-content">
            <div class="row">
              @foreach(App\Availablity::all() as $availablity)
                <div class="col-md-6">
                    <h5 class="no-margins">
                        <div class="i-checks">
                          <label>
                            <input type="checkbox" name="availablityid[]" @if(isset($selectedavailablity) && !empty($selectedavailablity) && in_array($availablity->id, $selectedavailablity))checked  @endif value="{{$availablity->id}}"> <i></i> 
                          </label> {{$availablity->name}} </div>
                    </h5>
                </div>
                @endforeach
            </div>
        </div>
    </div>
</div>
