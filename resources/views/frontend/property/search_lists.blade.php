@extends('layouts.default')

@section('content')

<div class="wrapper wrapper-content">
  <div class="col-md-12">
      <div class="ibox float-e-margins row">
          <div class="ibox-title selectedfilters">
                @include('frontend.property.filter.selectedfilters')
          </div>
      </div>
  </div>
  <form method="get" class="form-horizontal "  action="/property/search" enctype="multipart/form-data">
    {{ csrf_field()}}
        <div class="container">
          @include('frontend.property.filter')
          <div class="row col-lg-8" >
            <div class="row">
              <div class="col-md-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-content">
                            <span class="label label-primary pull-right"></span>
                            <h5>Selected Landmark <i class="fa fa-street-view"></i></h5>
                            <h7><strong>
                              <input type="text" id="searchlocation" value="@if(isset($selectedaddress)) {{$selectedaddress}} @elseif(isset($selectedkeyword)) {{$selectedkeyword}} @endif" class="form-control field"  placeholder="Your Desired Location"  aria-required="true" >
                    </div>
                  </div>
                </div>

                <!--div class="col-md-2">
                    <h5 class="no-margins">
                          <button  class="btn btn-success pull-right " style="margin-top:20px;">
                             Dummy<i class="fa fa-sort"></i></button>
                      </h5>
                </div -->
              </div>
                  @if($filteredproperty->count())
                  @foreach($filteredproperty as $property)
                    <div class="col-lg-12">
                        <div class="ibox float-e-margins">
                            <div class="ibox-content">
                                <div>
                                    <span class="pull-right text-right">
                                        <a href="/property/{{$property->id}}/viewproperty" class="tips btn btn-xs btn-white" data-toggle="tooltip" title="View Profile">
                                        <i class="fa fa-eye"></i>
                                        </a>
                                        @if(isset($property->wishlist))
                                         <a href="/manage/property/removewishlist/{{$property->id}}" class="tips btn btn-xs btn-white" data-toggle="tooltip" title="Remove from wshlisht" style="color:gold"><i class="fa fa-star"></i></a>
                                         @else
                                         <a href="/manage/property/wishlist/{{$property->id}}" class="tips btn btn-xs btn-white" data-toggle="tooltip" title="Add to wishlisht"><i class="fa fa-star-o"></i></a>
                                        @endif
                                    <br/>
                                    <div class="text-right">
                                    <!--div class="btn-group">
                                    <button class="btn btn-white btn-sm"><i class="fa fa-star"></i> Add to wishlist </button>
                                    <button class="btn btn-white btn-sm"><i class="fa fa-envelope"></i> Contact with Agent </button>
                                    </div-->
                                    </div>
                                    </span>
                                  <h3 class="font-bold no-margins">
                                      {{$property->name}}
                                  </h3>
                                  <hr>
                                  <small>
                                    <strong>
                                      {{$property->bedroom->name}}/{{$property->bedroom->rooms}} Bedroom(s)
                                    </strong>
                                  </small>
                                </div>

                                <div class="m-t-sm">

                                    <div class="row">
                                      <div class="col-md-12 row">

                                        <div class="col-md-8">
                                          <small><strong>Description:</strong></small><br>
                                            <div style="max-height:150px;overflow-y: auto;">
                                            {{$property->description}}
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                          <div class="product-images">
                                            @if($property->propertyImage->count())
                                            @foreach($property->propertyImage as $image)
                                              <div>
                                                  <div class="image-imitation" style="padding:0px;max-height: 200px;">
                                                    <img src="{{$image->name}}" style="width:100%;;height: 150px;">
                                                  </div>
                                              </div>
                                            @endforeach
                                            @else
                                            <div>
                                              <div class="image-imitation" style="padding:0px;max-height: 200px;">
                                                <img src="/assets/img/image-not-found.jpg" style="width:100%;;height: 150px;">
                                              </div>
                                              <p>
                                                  Sorry, but no images are available for this property yet.
                                                <br>
                                                <a href="">Request for image(s)...</a>
                                              </p>
                                            </div>
                                            @endif
                                          </div>
                                        </div>
                                      </div>
                                        <div class="col-md-12 row">

                                        <div class="col-md-4">
                                          <ul class="stat-list m-t-lg">
                                            <li><strong>Features:</strong>
                                              <ul>
                                            @foreach($property->feature as $feature)
                                                <li class="no-margins ">{{$feature->featurename->name}}</li >
                                            @endforeach
                                          </ul>
                                            </li>
                                        </ul>
                                        </div>
                                        <div class="col-md-4">
                                          <ul class="stat-list m-t-lg">
                                            <li><strong>Available For:</strong>
                                              <ul>
                                                @if(isset($property->availablity))
                                                @foreach($property->availablity as $availablity)
                                                <li class="no-margins ">{{$availablity->availablityname->name}}</li>
                                                @endforeach
                                                @else
                                                <li class="no-margins ">Availablity has not been mentioned.</li>
                                                @endif
                                          </ul>
                                            </li>
                                        </ul>
                                      </div>
                                      <div class="col-md-4">
                                        <ul class="stat-list m-t-lg">
                                          <li><strong>Services:</strong>
                                            <ul>
                                              @foreach($property->service as $service)
                                              <li class="no-margins ">{{$service->servicename->name}}</li>
                                              @endforeach
                                        </ul>
                                          </li>
                                      </ul>
                                    </div>
                                    <div class="col-md-4">
                                      <ul class="stat-list m-t-lg">
                                          <li>
                                              <h2 class="no-margins">{{$property->rent}} /- <small> in rupees</small></h2>
                                              <small>Monthly Rent</small>
                                          </li>
                                        </ul>
                                    </div>
                                    </div>
                                    </div>

                                </div>

                                <div class="m-t-md">
                                    <small class="pull-right">
                                        <i class="fa fa-clock-o"> </i>
                                        {{$property->created_at}}
                                    </small>
                                    <small>
                                        <strong></strong>
                                        <ul>
                                        </ul>
                                    </small>
                                </div>


                            </div>
                        </div>
                    </div>
                  @endforeach
                  @else

                      <div class="col-lg-12">
                          <div class="ibox float-e-margins">
                              <div class="ibox-content">
                                  <div class="m-t-sm">
                                      <div class="row">
                                        <div class="col-md-12 row">
                                            <center><h3><strong>@if(isset($message)){{$message}} !! @endif</strong></h3></center><br>
                                        </div>
                                      </div>
                                  </div>
                              </div>
                          </div>
                      </div>
                  @endif
                  </div>
            </div>
  </form>
</div>

@endsection
