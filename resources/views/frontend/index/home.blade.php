@extends('layouts.default')

@section('extra_style')
<style>
.property-search-toggle {
    background-color: transparent;
    left: 27%;
    margin-bottom: 0;
    margin-right: auto;
    margin-top: -30px;
    overflow-y: auto;
    padding-top: 0;
    position: absolute;
    top: 30%;
    width: 70%;
    z-index: 100;
    max-height: 1000px;
    cursor: pointer;
    padding: inherit;
}
.search-components{
  padding: 0px;
}
.search-components.search-list {
  width: 21%;
}

.search-components.search-box {
  float: left;
  width: 25%;
}
.search-box input {
  height: 53px;
}

</style>
@endsection
@section('content')

  <div class="row">  
    @include('frontend.search')     
    @include('frontend.frontend_map')
  </div>
@endsection


@section('extra_script')
<script>




</script>
@endsection
