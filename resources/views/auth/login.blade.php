@extends('layouts.app')

@section('content')

        <div class="col-md-6">
            <div class="ibox-content">
              <form class="m-t" role="form" method="POST" action="{{ url('/login') }}">
                  {{csrf_field()}}
                  <div class="form-group{{ $errors->has('username') ? ' has-error' : '' }}">
                    <input id="text" type="text" class="form-control" name="phone" placeholder="Your registerd phone number" value="{{ old('phone') }}" required>
                    @if ($errors->has('phone'))
                        <span class="help-block">
                            <strong>{{ $errors->first('phone') }}</strong>
                        </span>
                    @endif
                  </div>
                  <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                    <input id="password" type="password" class="form-control" placeholder="Password" name="password" required>
                    @if ($errors->has('password'))
                        <span class="help-block">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                    @endif
                  </div>
                  <div class="form-group">
                    <div class="i-checks">
                        <label>
                            <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : ''}}> Remember Me
                        </label>
                    </div>
                  </div>
                  <button type="submit" class="btn btn-primary block full-width m-b">Login</button>

                  <a href="{{ url('/password/reset') }}"><small>Forgot password?</small></a>


                  <p class="text-muted text-center"><small>Do not have an account?</small></p>

                  <a class="btn btn-sm btn-white btn-block" href="{{ url('/register') }}">Create an account</a>
                </form>
                <p class="m-t">
                  <small>“Home isn't where you're from, it's where you find light when all grows dark.”
                    -- Digital Property &copy; 2016</small>
                </p>
            </div>
        </div>


@endsection
