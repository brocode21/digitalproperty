@extends('layouts.app')

@section('content')

  <div class="col-md-6">
          <h3>Register to Digital Property</h3>
          <p>Create account to see it in action.</p>
          <form class="m-t"  role="form" method="POST" action="{{ url('/register') }}">
              {{ csrf_field() }}
              <div class="form-group{{ $errors->has('phone') ? ' has-error' : '' }}">
                <input id="text" type="text" class="form-control" name="phone" placeholder="Mobile No." value="{{ old('phone') }}" required>
                @if ($errors->has('phone'))
                    <span class="help-block">
                        <strong>{{ $errors->first('phone') }}</strong>
                    </span>
                @endif
              </div>
              <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                  <input id="password" type="password" class="form-control" placeholder="Password" name="password" required>

                  @if ($errors->has('password'))
                      <span class="help-block">
                          <strong>{{ $errors->first('password') }}</strong>
                      </span>
                  @endif
              </div>
              <div class="form-group">
                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" placeholder="Confirm Password" required>
              </div>
              <div class="form-group">
                <div class="checkbox i-checks">
                  <label> <input type="checkbox" name="agreement" required><i></i>I Agree to the terms and policy. </label>
                </div>
              </div>
              <div class="form-group">
                <div class="g-recaptcha" data-sitekey="{{ env('RE_CAP_SITE') }}" style="margin-left: 25px;"></div>
              </div>
              <button type="submit" class="btn btn-primary block full-width m-b">Register</button>

              <p class="text-muted text-center"><small>Already have an account?</small></p>
              <a class="btn btn-sm btn-white btn-block" href="{{ url('/login') }}">Login</a>
          </form>
  </div>

@endsection
