<div class="col-md-6">
    <h3 class="font-bold">Welcome to Digital Property</h3>


    <p>
      “What is home? My favorite definition is "a safe place," a place where one is free from attack, a place where one experiences secure relationships and affirmation. It's a place where people share and understand each other. Its relationships are nurturing. The people in it do not need to be perfect; instead, they need to be honest, loving, supportive, recognizing a common humanity that makes all of us vulnerable.”
      ― <strong>Gladys M. Hunt, Honey for a Child's Heart: The Imaginative Use of Books in Family Life</strong>     </p>

    <p>
      “The ache for home lives in all of us. The safe place where we can go as we are and not be questioned.”
      ― <strong>Maya Angelou, All God's Children Need Traveling Shoes</strong>     </p>

    <p>
      John le Carré
      “Home's where you go when you run out of homes.”
      ― <strong>John le Carré, The Honourable Schoolboy </strong>    </p>



</div>
