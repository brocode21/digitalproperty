<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>Digital Properties | we find your homes.</title>

    <link href="/assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="/assets/font-awesome/css/font-awesome.css" rel="stylesheet">
    <link href="/assets/css/plugins/iCheck/custom.css" rel="stylesheet">

    <link href="/assets/css/animate.css" rel="stylesheet">
    <link href="/assets/css/style.css" rel="stylesheet">

    <!-- Scripts -->
    <script>
        window.Laravel = <?php echo json_encode([
            'csrfToken' => csrf_token(),
        ]); ?>
    </script>

</head>

<body class="gray-bg">
  <div class="loginColumns animated fadeInDown">
    <div class="row">

        @include('auth.auth_includes.welcome_note')

        @yield('content')

        </div>
    <hr/>
    <div class="row">
        <div class="col-md-6">
        <small>Digital Property </small>
        </div>
        <div class="col-md-6 text-right">
           <small>© 2016-2017</small>
        </div>
    </div>
  </div>

  <!-- iCheck -->
  <script src='https://www.google.com/recaptcha/api.js'></script>

  <script src="/assets/js/plugins/iCheck/icheck.min.js"></script>

    <!-- Scripts -->
    <script>
        $(document).ready(function () {
            $('.i-checks').iCheck({
                checkboxClass: 'icheckbox_square-green',
                radioClass: 'iradio_square-green',
            });
        });
    </script>
</body>
</html>
