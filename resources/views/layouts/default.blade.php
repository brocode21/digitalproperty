<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>Digital Properties | we find your homes.</title>

    <link href="/assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="/assets/font-awesome/css/font-awesome.css" rel="stylesheet">
    <link href="/assets/css/plugins/iCheck/custom.css" rel="stylesheet">
    <link href="/assets/css/animate.css" rel="stylesheet">
    <link href="/assets/css/style.css" rel="stylesheet">
    <link href="css/plugins/blueimp/css/blueimp-gallery.min.css" rel="stylesheet">
    <link href="/assets/css/plugins/slick/slick.css" rel="stylesheet">
    <link href="/assets/css/plugins/slick/slick-theme.css" rel="stylesheet">
    <link href="/assets/css/plugins/dropzone/basic.css" rel="stylesheet">
    <link href="/assets/css/plugins/dropzone/dropzone.css" rel="stylesheet">
    <link href="/assets/css/plugins/blueimp/css/blueimp-gallery.min.css" rel="stylesheet">

    <link href='http://fonts.googleapis.com/css?family=Roboto:300,400,700' rel='stylesheet' type='text/css'>
    @yield('extra_style')

    <style>
    .gmnoprint {
    float: right !important;
    position: sticky !important;
    right: 0;
  }

  .search-property-btn {
    border-bottom-left-radius: 0;
    border-bottom-right-radius: 0;
    border-top-left-radius: 0;
    border-top-right-radius: 0;
    font-size: 17px;
    font-weight: bolder;
    padding-bottom: 11px;
    padding-left: 30px;
    padding-right: 30px;
    padding-top: 14px;
  }

  .search-property-btn .fa-search{
    font-size: 24px;

  }
    </style>

</head>

<body class="top-navigation">

      <div id="wrapper">
        <div id="page-wrapper" class="gray-bg">
          @include('includes.top_navbar')

          <div class="wrapper wrapper-content" style="padding:0px !important;margin:0px !important;">
            @yield('content')
          </div>
          <center>Copyright (c) 2017 Copyright digitalProperty All Rights Reserved.</center>


              @php
              if(Auth::check())
                Auth::user()->onlineStatus();
              @endphp
        </div>
        @include('includes.small_chat')
        @include('includes.right_bar')
      </div>

    <!-- Mainly scripts -->
    <script src="/assets/js/jquery-2.1.1.js"></script>
    <script src="/assets/js/bootstrap.min.js"></script>
    <script src="/assets/js/plugins/metisMenu/jquery.metisMenu.js"></script>
    <script src="/assets/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>

    <!-- Custom and plugin javascript -->
    <script src="/assets/js/plugins/pace/pace.min.js"></script>
    <script src="/assets/js/inspinia.js"></script>

    <!-- slick carousel-->
    <script src="/assets/js/plugins/slick/slick.min.js"></script>
    <!-- DROPZONE -->
    <script src="/assets/js/plugins/dropzone/dropzone.js"></script>

    <!-- iCheck -->
    <script src="/assets/js/plugins/iCheck/icheck.min.js"></script>
    <!-- Flot -->
    <script src="/assets/js/plugins/flot/jquery.flot.js"></script>
    <script src="/assets/js/plugins/flot/jquery.flot.tooltip.min.js"></script>
    <script src="/assets/js/plugins/flot/jquery.flot.resize.js"></script>

     <!-- Peity -->
    <script src="/assets/js/plugins/peity/jquery.peity.min.js"></script>
     <!-- Peity demo -->
    <script src="/assets/js/demo/peity-demo.js"></script>
    <!-- DROPZONE -->
    <script src="/assets/js/plugins/dropzone/dropzone.js"></script>

    <!-- blueimp gallery -->
    <script src="/assets/js/plugins/blueimp/jquery.blueimp-gallery.min.js"></script>

    <style>
        /* Local style for demo purpose */

        .lightBoxGallery {
            text-align: center;
        }

        .lightBoxGallery img {
            margin: 5px;
        }

    </style>


    <!-- Page-Level Scripts -->
    <script>
    $(document).ready(function(){
        $('.product-images').slick({
            dots: true
        });
    });
    $(document).ready(function(){
        $('[data-toggle="tooltip"]').tooltip({placement: "bottom"});
    });

    var latitude = 30.7333;
    var longitude =76.7794;

    $('.left-collapse-link').click(function () {
        var ibox = $(this).closest('div.ibox');
        var button = $(this).find('i');
        var content = ibox.find('div.ibox-content');
        content.slideToggle(500);
        button.toggleClass('fa-chevron-down').toggleClass('fa-chevron-up');
        ibox.toggleClass('').toggleClass('border-bottom');
        setTimeout(function () {
            ibox.resize();
            ibox.find('[id^=map-]').resize();
        }, 50);
    });



    </script>
     <script>
         $(document).ready(function () {
             $('.i-checks').iCheck({
                 checkboxClass: 'icheckbox_square-green',
                 radioClass: 'iradio_square-green',
             });
         });
     </script>

    @yield('extra_script')
    <script src="/js/maps-create.js"></script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBOU22QQFaxW595x4W-OdXbyLz1BgeSX4U&libraries=places&callback=initAutocomplete"></script>
    <script src="/js/maps-view.js"></script>
    <script src="/js/maps-home.js"></script>

    <script>
        $(document).ready(function(){

            Dropzone.options.myAwesomeDropzone = {

                autoProcessQueue: false,
                uploadMultiple: true,
                parallelUploads: 100,
                maxFiles: 100,
                addRemoveLinks: true,

                // Dropzone settings
                init: function() {
                    var myDropzone = this;

                    this.element.querySelector("button[type=submit]").addEventListener("click", function(e) {
                        e.preventDefault();
                        e.stopPropagation();
                        myDropzone.processQueue();
                    });
                    this.on("sendingmultiple", function() {
                    });
                    this.on("successmultiple", function(files, response) {
                    });
                    this.on("errormultiple", function(files, response) {
                    });
                }

            }

       });

    </script>

</body>

</html>
