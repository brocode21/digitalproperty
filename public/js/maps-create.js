

  function initAutocomplete() {
    var my_position = new google.maps.LatLng( latitude, longitude);
    var map = new google.maps.Map(document.getElementById('map1'), {
      center: my_position,
      disableDoubleClickZoom: false,
      zoom: 8,
      styles: [{"featureType":"water","stylers":[{"saturation":43},{"lightness":-11},{"hue":"#0088ff"}]},{"featureType":"road","elementType":"geometry.fill","stylers":[{"hue":"#ff0000"},{"saturation":-100},{"lightness":99}]},{"featureType":"road","elementType":"geometry.stroke","stylers":[{"color":"#808080"},{"lightness":54}]},{"featureType":"landscape.man_made","elementType":"geometry.fill","stylers":[{"color":"#ece2d9"}]},{"featureType":"poi.park","elementType":"geometry.fill","stylers":[{"color":"#ccdca1"}]},{"featureType":"road","elementType":"labels.text.fill","stylers":[{"color":"#767676"}]},{"featureType":"road","elementType":"labels.text.stroke","stylers":[{"color":"#ffffff"}]},{"featureType":"poi","stylers":[{"visibility":"off"}]},{"featureType":"landscape.natural","elementType":"geometry.fill","stylers":[{"visibility":"on"},{"color":"#b8cb93"}]},{"featureType":"poi.park","stylers":[{"visibility":"on"}]},{"featureType":"poi.sports_complex","stylers":[{"visibility":"on"}]},{"featureType":"poi.medical","stylers":[{"visibility":"on"}]},{"featureType":"poi.business","stylers":[{"visibility":"simplified"}]}]
    });
    var input = document.getElementById('autocomplete');
    var options = {
                    types: ['geocode'],
                    componentRestrictions: {country: 'IN'}
                };
    var autocomplete = new google.maps.places.Autocomplete(input,options);

    var infowindow = new google.maps.InfoWindow();
    var infowindowContent = document.getElementById('infowindow-content');
    infowindow.setContent(infowindowContent);
    var marker = new google.maps.Marker({
      map: map,
      position: my_position,
      title: 'Drag Me!',
      clickable:true,
      draggable: true,
      animation: google.maps.Animation.DROP,
      anchorPoint: new google.maps.Point(0, -29)
    });

    google.maps.event.addListener(map, 'click', function(event) {
          var positionclick = event.latLng;
          marker.setPosition(positionclick);
          maplocation(event);
          locationname(event);
        // if you don't do this, the map will zoom in
    });

    google.maps.event.addListener(marker, 'dragend', function(event) {
        maplocation(event);
        locationname(event);
    });

    function maplocation(event){
        console.log(event.latLng.lat());
          //document.getElementById('lat').value = event.latLng.lat();
          //document.getElementById('long').value = event.latLng.lng();

    }

    function locationname(event){
        var geocoder  = new google.maps.Geocoder();             // create a geocoder object
        var location  = new google.maps.LatLng(event.latLng.lat(), event.latLng.lng());    // turn coordinates into an object
        geocoder.geocode({'latLng': location}, function (results, status) {
        if(status == google.maps.GeocoderStatus.OK) {
          var address = [];
          for (var i = 0; i < results[0].address_components.length; i++) {
              var addressTypeval = results[0].address_components[i].long_name;
              var addressType = results[0].address_components[i].types[0];
               address.push(results[0].address_components[i].long_name);
               if(addressType == "postal_code"){
                 document.getElementById("postal_code").value = addressTypeval;
               }
        }

      
        document.getElementById('lat').value = event.latLng.lat();
        document.getElementById('long').value = event.latLng.lng();
        document.getElementsByName("address")[0].value = address;
        document.getElementsByName("formattedaddress")[0].value = results[0].formatted_address;         // if address found, pass to processing function
         // if address found, pass to processing function
        document.write(results[0].formatted_address);
          }
        });
    }

    autocomplete.addListener('place_changed', function() {
      infowindow.close();
      marker.setVisible(false);
      var place = autocomplete.getPlace();
      if (!place.geometry) {
        window.alert("No details available for input: '" + place.name + "'");
        return;
      }

      // If the place has a geometry, then present it on a map.
      if (place.geometry.viewport) {
        map.fitBounds(place.geometry.viewport);
      } else {
        map.setCenter(place.geometry.location);
        map.setZoom(17);  // Why 17? Because it looks good.
      }
      marker.setPosition(place.geometry.location);
      marker.setVisible(true);

      var address = [];
      for (var i = 0; i < place.address_components.length; i++) {
            var addressTypeval = place.address_components[i].long_name;
            var addressType = place.address_components[i].types[0];
            address.push(place.address_components[i].long_name);
           if(addressType == "postal_code"){
             document.getElementById("postal_code").value = addressTypeval;
           }
     }
     ///data entry to form
     document.getElementsByName("address")[0].value = address;
     document.getElementsByName("formattedaddress")[0].value = place.formatted_address;
     document.getElementById('lat').value = place.geometry.location.lat();
     document.getElementById('long').value = place.geometry.location.lng();

      var addressinfowindowContent = '';
      if (place.address_components) {
        address = [
          (place.address_components[0] && place.address_components[0].short_name || ''),
          (place.address_components[1] && place.address_components[1].short_name || ''),
          (place.address_components[2] && place.address_components[2].short_name || '')
        ].join(' ');
        infowindowContent.children['place-icon'].src = place.icon;
        infowindowContent.children['place-name'].textContent = place.name;
        infowindowContent.children['place-address'].textContent = addressinfowindowContent;
        infowindow.open(map, marker);
      }




    });

    // Sets a listener on a radio button to change the filter type on Places
    // Autocomplete.

  }
