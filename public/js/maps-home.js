/*
$('body').on('click','#decoder ',function(){
  var lat = '';
    var lng = '';
    var address = '160071';
    geocoder =new google.maps.Geocoder();
    geocoder.geocode( { 'address': address}, function(results, status) {
      if (status == google.maps.GeocoderStatus.OK) {
        console.log(results);
         lat = results[0].geometry.location.lat();
         lng = results[0].geometry.location.lng();
      } else {
        alert("Geocode was not successful for the following reason: " + status);
      }
      alert('Latitude: ' + lat + ' Logitude: ' + lng);
    });
});
*/


getproperties();
autocomplete();

var my_position = new google.maps.LatLng( latitude, longitude);

var map = new google.maps.Map(document.getElementById('map3'), {
  center: my_position,
  disableDoubleClickZoom: false,
  zoom:4,
  styles: [{"featureType":"water","stylers":[{"saturation":43},{"lightness":-11},{"hue":"#0088ff"}]},{"featureType":"road","elementType":"geometry.fill","stylers":[{"hue":"#ff0000"},{"saturation":-100},{"lightness":99}]},
  {"featureType":"road","elementType":"geometry.stroke","stylers":[{"color":"#808080"},{"lightness":54}]},{"featureType":"landscape.man_made","elementType":"geometry.fill","stylers":[{"color":"#ece2d9"}]},
  {"featureType":"poi.park","elementType":"geometry.fill","stylers":[{"color":"#ccdca1"}]},
  {"featureType":"road","elementType":"labels.text.fill","stylers":[{"color":"#767676"}]},
  {"featureType":"road","elementType":"labels.text.stroke","stylers":[{"color":"#ffffff"}]},
  {"featureType":"poi","stylers":[{"visibility":"off"}]},
  {"featureType":"landscape.natural","elementType":"geometry.fill","stylers":[{"visibility":"on"},{"color":"#b8cb93"}]},
  {"featureType":"poi.park","stylers":[{"visibility":"on"}]},
  {"featureType":"poi.sports_complex","stylers":[{"visibility":"on"}]},
  {"featureType":"poi.medical","stylers":[{"visibility":"on"}]},
  {"featureType":"poi.business","stylers":[{"visibility":"simplified"}]}]
});

function autocomplete() {



  var input = document.getElementById('searchlocation');
  var options = {
                  types: ['geocode'],
                  componentRestrictions: {country: 'IN'}
              };
  var autocomplete = new google.maps.places.Autocomplete(input,options);
  autocomplete.addListener('place_changed', function() {
    var place = autocomplete.getPlace();
    if (!place.geometry) {
      //window.alert("No details available for input: '" + place.name + "'");
      return;
    }
    if(map){
      if (place.geometry.viewport) {
        map.fitBounds(place.geometry.viewport);
      } else {
        map.setCenter(place.geometry.location);
        map.setZoom(10);  // Why 17? Because it looks good.
      }
    }

    var address = [];
    for (var i = 0; i < place.address_components.length; i++) {
          var addressTypeval = place.address_components[i].long_name;
          var addressType = place.address_components[i].types[0];
          address.push(place.address_components[i].long_name);
   }
   console.log(place.geometry.location.lat());
   document.getElementsByName("address")[0].value = address;
   document.getElementsByName("lat")[0].value = place.geometry.location.lat();
   document.getElementsByName("long")[0].value = place.geometry.location.lng();
   document.getElementsByName("formattedaddress")[0].value = place.formatted_address;
  });
}
function getproperties(){
  $.ajax({
     url: '/getproperties',
     dataType:'json',
     success: function(data) {

       for (var i = 0; i < data.properties.length; i++) {
           var propertyid = data.properties[i].id;
           var latitude = data.properties[i].lat;
           var longitude = data.properties[i].long;
           var my_position = new google.maps.LatLng( latitude, longitude);
           var marker = new google.maps.Marker({
             position: my_position,
             map: map,
             title: data.properties[i].formattedaddress,
             clickable:true,
             draggable: false,
             animation: google.maps.Animation.DROP,
           });
         }
     }

   });


}



var minZoomLevel = 5;
var maxZoomLevel = 15;

/*
   var map = new google.maps.Map(document.getElementById('map3'), {
      zoom: 5,
      center: new google.maps.LatLng(22, 77),
      mapTypeId: google.maps.MapTypeId.ROADMAP
   });

   // Bounds for India
   var strictBounds = new google.maps.LatLngBounds(

     new google.maps.LatLng(35,97), //NE
     new google.maps.LatLng(8,70)  //SW
   );

   // Listen for the dragend event
   google.maps.event.addListener(map, 'dragend', function(event) {
     strictboundcheck(event);
   });
   // Listen for the click event
   google.maps.event.addListener(map, 'click', function(event) {
     strictboundcheck(event);
   });
   /*
   google.maps.event.addListener(map, 'zoom_changed', function(event) {
     strictboundcheck(event);
   });

   // Listen for the dblclick event
   google.maps.event.addListener(map, 'dblclick', function(event) {
     strictboundcheck(event);
   });

   function strictboundcheck(event)
   {
     if (strictBounds.contains(map.getCenter())) return;

     // We're out of bounds - Move the map back within the bounds

     var c = map.getCenter(),
         x = c.lng(),
         y = c.lat(),

         maxX = strictBounds.getNorthEast().lng(),
         maxY = strictBounds.getNorthEast().lat(),
         minX = strictBounds.getSouthWest().lng(),
         minY = strictBounds.getSouthWest().lat();
         //console.log(maxX);         console.log(maxY);
         //console.log(minX);         console.log(minY);

         //console.log(x);console.log(y);

     if (x < minX) x = minX;
     if (x > maxX) x = maxX;
     if (y < minY) y = minY;
     if (y > maxY) y = maxY;

     map.setCenter(new google.maps.LatLng(y, x));
   }

   // Limit the zoom level
   google.maps.event.addListener(map, 'zoom_changed', function() {
     if (map.getZoom() < minZoomLevel) map.setZoom(minZoomLevel);
   });

   */
