/*
$('body').on('click','#decoder ',function(){
  var lat = '';
    var lng = '';
    var address = '160071';
    geocoder =new google.maps.Geocoder();
    geocoder.geocode( { 'address': address}, function(results, status) {
      if (status == google.maps.GeocoderStatus.OK) {
        console.log(results);
         lat = results[0].geometry.location.lat();
         lng = results[0].geometry.location.lng();
      } else {
        alert("Geocode was not successful for the following reason: " + status);
      }
      alert('Latitude: ' + lat + ' Logitude: ' + lng);
    });
});
*/


function initialize() {
  var my_position = new google.maps.LatLng( latitude, longitude);
  var map = new google.maps.Map(document.getElementById('map2'), {
    center: my_position,
    disableDoubleClickZoom: false,
    zoom: 13,
    styles: [{"featureType":"water","stylers":[{"saturation":43},{"lightness":-11},{"hue":"#0088ff"}]},{"featureType":"road","elementType":"geometry.fill","stylers":[{"hue":"#ff0000"},{"saturation":-100},{"lightness":99}]},{"featureType":"road","elementType":"geometry.stroke","stylers":[{"color":"#808080"},{"lightness":54}]},{"featureType":"landscape.man_made","elementType":"geometry.fill","stylers":[{"color":"#ece2d9"}]},{"featureType":"poi.park","elementType":"geometry.fill","stylers":[{"color":"#ccdca1"}]},{"featureType":"road","elementType":"labels.text.fill","stylers":[{"color":"#767676"}]},{"featureType":"road","elementType":"labels.text.stroke","stylers":[{"color":"#ffffff"}]},{"featureType":"poi","stylers":[{"visibility":"off"}]},{"featureType":"landscape.natural","elementType":"geometry.fill","stylers":[{"visibility":"on"},{"color":"#b8cb93"}]},{"featureType":"poi.park","stylers":[{"visibility":"on"}]},{"featureType":"poi.sports_complex","stylers":[{"visibility":"on"}]},{"featureType":"poi.medical","stylers":[{"visibility":"on"}]},{"featureType":"poi.business","stylers":[{"visibility":"simplified"}]}]
  });
  var marker = new google.maps.Marker({
    position: my_position,
    map: map,
    title: 'this is it!',
    clickable:true,
    draggable: false,
    animation: google.maps.Animation.DROP,
  });
}
google.maps.event.addDomListener(window, 'load', initialize);
