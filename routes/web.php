<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
*/

Auth::routes();

Route::get('/', 'FrontendController@index');
Route::get('getproperties', 'FrontendController@getproperties');
Route::get('allproperty', 'PropertyController@allproperty');
Route::get('/property/search', 'PropertyController@searchproperty');
Route::get('property/{id}/viewproperty', 'PropertyController@viewproperty');
Route::group(['middleware'=>'auth'],function()
{
  /*Property Routes*/
  Route::group(['prefix'=>'manage'],function()
  {
    Route::get('/property/view', 'PropertyController@index');
    Route::get('/property/create', 'PropertyController@createproperty');
    Route::post('/property/store', 'PropertyController@storeproperty');
    Route::post('/property/imagestore', 'PropertyController@storepropertyimages');
    Route::get('/property/{id}/editproperty', 'PropertyController@editproperty');
    Route::post('/property/update', 'PropertyController@updateproperty');
    Route::get('/property/{id}/delete', 'PropertyController@deleteproperty');
    Route::get('/property/{id}/viewproperty', 'PropertyController@viewproperty');
    //
    Route::get('/property/wishlist/{id}', 'PropertyController@wishlist');
    Route::get('/property/removewishlist/{id}', 'PropertyController@removewishlist');

    /*Manage users Routes*/
    Route::get('user', 'UsersController@index');
    Route::get('/user/{id}/viewprofile', 'UsersController@viewprofile');
    Route::get('/user/{id}/viewprofile/{propertyId}/viewallpropertyimages', 'UsersController@viewallpropertyimages');
    Route::get('/user/{id}/delete', 'UsersController@delete');
    Route::get('/user/{id}/accessprofile', 'UsersController@accessprofile');
  });
});
