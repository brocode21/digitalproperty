<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PropertyService extends Model
{
    //
    public function servicename()
    {
      return $this->hasOne('App\Service','id','service_id');
    }
}
