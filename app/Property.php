<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Auth;

class Property extends Model
{
    //
    public function availablity()
    {
      return $this->hasMany('App\PropertyAvailablity','property_id','id');
    }

    public function postedBy()
    {
      return $this->hasOne('App\User','id','agent_id');
    }

    public function bedroom()
    {
      return $this->hasOne('App\Bedroom','id','bedroom_id');
    }

    public function feature()
    {
      return $this->hasMany('App\PropertyFeature','property_id','id');
    }

    public function service()
    {
      return $this->hasMany('App\PropertyService','property_id','id');
    }
    public function propertyImage()
    {
      return $this->hasMany('App\PropertyImage','property_id','id')->orderby('id','desc');
    }
    public function wishlist()
    {
      return $this->hasOne('App\Wishlist','property_id','id')->where('user_id',Auth::id());
    }
    
}
