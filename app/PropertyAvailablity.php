<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PropertyAvailablity extends Model
{
    //
    public function availablityname()
    {
      return $this->hasOne('App\Availablity','id','availablity_id');
    }

}
