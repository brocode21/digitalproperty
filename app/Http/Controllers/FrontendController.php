<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Property;
use App\User;
use Auth;

class FrontendController extends Controller
{
    public function index()
    {
        $data['rents'] =  Property::all()->pluck('rent','id');
        return view('frontend.index.home',$data);
    }

    public function getproperties()
    {
      $data['properties'] =  \DB::table('properties')->select('id','formattedaddress','lat', 'long')->get();

      return \Response::json($data);

    }
}
