<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Property;
use App\PropertyFeature;
use App\PropertyAvailablity;
use App\PropertyService;
use App\PropertyImage;
use Auth;
use App\Wishlist;

class PropertyController extends Controller
{
    //
    public function index()
    {
      $data['properties'] = Property::where('agent_id',Auth::id())->orderby('rent')->get();

      return view ('admin.property.index',$data);
    }
    public function allproperty()
    {
      $data['filteredproperty'] = Property::orderby('rent')->get();
      $data['rents'] =  Property::orderby('rent','asc')->pluck('rent','id');
      $data['message'] = 'SORRY, WE ARE OUT OF PROPERTIES';

      return view ('frontend.property.search_lists',$data);
    }

    public function searchproperty(Request $request)
    {
      /*
      SQLSTATE[42000]: Syntax error or access violation: 1066 Not unique table/alias: 'properties'
      (SQL: select * from `properties` left join `properties` on `property_features`.`property_id` = `properties`.`id`)
      */
      /*
      $data['allproperty'] = Property::leftjoin('property_availablities as PropAvail','PropAvail.property_id','properties.id')
                                      ->leftJoin('property_features as PropFeature','PropFeature.property_id','properties.id')
                                      //->leftjoin('property_services as PropServices','PropServices.property_id','properties.id')
                                      ->select('properties.*','PropFeature.feature_id','PropAvail.availablity_id')
                                      ->get();
      */
        //echo "<pre>";
        //////////////replace propertysearch keyword india
        $property = new Property;

        $maxRent = !empty($request->max_rent) ? $request->max_rent:'';
        $minRent = !empty($request->min_rent) ? $request->min_rent:'';
        $bedrooms = !empty($request->bedrooms) ? $request->bedrooms : '';
        $availablity = !empty($request->availablityid) ? $request->availablityid : '';
        $address = !empty($request->address)?$request->address:'';
        $keyword = !empty($request->desiredkeyword)?$request->desiredkeyword:'';
        $bedrooms = !empty($request->bedrooms)?$request->bedrooms:'';
        $services = !empty($request->services)?$request->services:'';
        $features = !empty($request->features)?$request->features:'';

        if ((!empty($minRent) || !empty($maxRent)) || !empty($address) || !empty($keyword) || is_array($availablity) || is_array($bedrooms) || is_array($services) ||  is_array($features))
        {
          $property = $this->filterrent($property,$minRent,$maxRent);

          $property = $this->filterkeyword($property,$address,$keyword);

          $property = $this->filterbedrooms($property,$bedrooms);

          $property = $this->filterkeyword($property,$address,$keyword);

          $property = $this->filteravailablity($property,$availablity);

          $property = $this->filterservices($property,$services);

          $property = $this->filterfeatures($property,$features);


        }
        /*

                $propertyavail = $property::join('property_availablities as PropAvail','PropAvail.availabilty_id','properties.id')
                                    ->select('properties.*','PropAvail.availabilty_id');
                $propertyservices = $property::join('property_services as PropService','PropService.service_id','properties.id')
                                    ->select('properties.*','PropService.service_id');
                $propertyfeatures = $property::join('property_features as PropFeature','PropFeature.feature_id','properties.id')
                                    ->select('properties.*','PropFeature.feature_id');
                $propertyavail = $property->with('availablity');


        //#1for rent request only  ----working----
        if ((!empty($minRent) || !empty($maxRent)) && empty($address) && empty($keyword) && empty($availablity))
        {
          $property = $this->filterrent($property,$minRent,$maxRent);
        }
        //#2for availablity request only   ---working-----
        elseif (is_array($availablity) && empty($minRent) && empty($maxRent) && empty($address) && empty($keyword))
        {
          $property = $this->filteravailablity($propertyavail,$availablity);
        }
        //#3for keyword only   --working---
        elseif ((!empty($address) || !empty($keyword)) && empty($availablity) && empty($minRent) && empty($maxRent))
        {
          //echo "keywordcheck";
          $property = $this->filterkeyword($property,$address,$keyword);
        }
        //#4for availablity and rent request together --working---
        elseif (is_array($availablity) &&  empty($address) && empty($keyword) && ((!empty($minRent) || !empty($maxRent))) )
        {
            //echo "availablitycheck & rentcheck";
          $property = $this->filteravailablity($propertyavail,$availablity);
          $property = $this->filterrent($property,$minRent,$maxRent);

        }
        //#5for availablity and keyword request together --working--
        elseif ($availablity && ((!empty($address) || !empty($keyword))) && empty($minRent) && empty($maxRent) )
        {
          //echo "availablitycheck & keywordcheck";
          $property = $this->filteravailablity($propertyavail,$availablity);
          $property = $this->filterkeyword($property,$address,$keyword);

        }
        //#6for rent and keyword request together --working--
        elseif  ((!empty($minRent) || !empty($maxRent)) && !empty($address) || !empty($keyword) && empty($availablity))
        {
            //echo "rentcheck & keywordcheck";
          $property = $this->filterrent($property,$minRent,$maxRent);
          $property = $this->filterkeyword($property,$address,$keyword);
        }
        //#7for rent, availablity and keyword request together  --working--
        elseif ((!empty($minRent) || !empty($maxRent)) && (!empty($address) || !empty($keyword)) && is_array($availablity))
        {
              //echo "rentcheck, availablecheck & keyword check";
          $property = $this->filteravailablity($property,$availablity);
          $property = $this->filterrent($property,$minRent,$maxRent);
          $property = $this->filterkeyword($property,$address,$keyword);

        }
        elseif (is_array($bedrooms) && empty($availablity) && empty($minRent) && empty($maxRent) && empty($address) && empty($keyword) &&empty($services))
        {
          $property = $this->filterbedrooms($property,$bedrooms);
        }
        elseif (is_array($services) && empty($bedrooms) && empty($availablity) && empty($minRent) && empty($maxRent) && empty($address) && empty($keyword))
        {
          $property = $this->filterservices($propertyservices,$services);
        }
        elseif (is_array($features) && empty($services)&& empty($bedrooms) && empty($availablity) && empty($minRent) && empty($maxRent) && empty($address) && empty($keyword))
        {
          $property = $this->filterfeatures($propertyfeatures,$features);
        }
        //#8 for bedrooms,services check   --working--
        elseif (is_array($bedrooms) && !empty($services) && empty($availablity) && empty($minRent) && empty($maxRent) && empty($address) && empty($keyword))
        {

          $property = $this->filterbedrooms($property,$bedrooms);
          $property = $this->filterservices($propertyservices,$services);
        }*/
        else
        {
          return \Redirect::back();
        }

        //passing selected filters to next property search page
        $data['maxRent'] = $maxRent;
        $data['minRent'] = $minRent;
        $data['selectedavailablity'] = $availablity;
        $data['selectedaddress'] = $address;
        $data['selectedkeyword'] = $keyword;
        $data['selectedbedrooms'] = $bedrooms;
        $data['selectedservices'] = $services;
        $data['selectedfeatures'] = $features;

        $data['rents'] =  Property::orderby('rent','asc')->pluck('rent','id');
        if($property->count() == 0)
        {
          $data['message'] = 'SORRY, AS WE COULD NOT FIND MATCHES TO YOUR SELECTED CHOICE(S) FOR PROPERTIES';
          $data['filteredproperty'] = $property;
          return view('frontend.property.search_lists',$data);
        }
        //print_r($property);die;
        //for desired keyword only
        /*elseif ($request->desiredkeyword || (empty($request->availablityid) && (empty($minRent) && empty($maxRent)))) {
          $data['property'] = PropertyAvailablity::join('availablities','property_availablities.availablity_id','availablities.id')
                                                  ->join('properties','property_availablities.property_id','properties.id')
                                                  ->orwhere('description', 'like', '%' . $request->desiredkeyword . '%')
                                                  ->orwhere('address', 'like', '%' . $request->desiredkeyword . '%')
                                                  ->orwhere('formattedaddress', 'like', '%' . $request->desiredkeyword . '%')
                                                  ->get();
        }*/

        if (isset($request->sorting)) {
            if ($request->sorting == '2')
                $property = $property->orderBy('rent', 'asc');
            else if ($request->sorting == '3')
                $property = $property->orderBy('rent', 'desc');
            else if ($request->sorting == '1')
                $property = $property->orderBy('created_at', 'desc');
            $data['selectedsorting'] = $request->sorting;
        }


        $data['filteredproperty'] = $property->get()->unique('id');
        return view('frontend.property.search_lists',$data);
    }

    public function createproperty()
    {
      return view ('admin.property.createproperty');
    }

    public function storeproperty(Request $request)
    {

      $newproperty = new Property;
      $newproperty->agent_id = $request->agent_id;
      $newproperty->name = $request->name;
      $newproperty->description = $request->description;
      $newproperty->rent = $request->rent;
      $newproperty->property_type_id = $request->property_type_id;
      $newproperty->address = $request->address;
      $newproperty->postal_code = $request->postal_code;
      $newproperty->lat = $request->lat;
      $newproperty->long = $request->long;
      $newproperty->formattedaddress = $request->formattedaddress;
      $newproperty->bedroom_id = $request->bedroom_id;
      $newproperty->credential_id = $request->credential_id;
      $newproperty->save();

      $data['property_id']=$newproperty->id;

       if(is_array($request->featureid))
       {
         foreach ($request->featureid as $key => $featureid)
         {
           $newfeature = new PropertyFeature;
           $newfeature->property_id = $newproperty->id;
           $newfeature->feature_id = $featureid;
           $newfeature->save();
         }
       }
       if(is_array($request->availablityid))
       {
         foreach ($request->availablityid as $key => $availablityid)
         {
           $availablity = new PropertyAvailablity;
           $availablity->property_id = $newproperty->id;
           $availablity->availablity_id = $availablityid;
           $availablity->save();
         }
       }
       if(is_array($request->serviceid))
       {
         foreach ($request->serviceid as $key => $serviceid)
         {
           $service = new PropertyService;
           $service->property_id = $newproperty->id;
           $service->service_id = $serviceid;
           $service->save();
         }
       }
      return view ('admin.property.createpropertyimage',$data);
    }

    public function storepropertyimages(Request $request)
    {

      if(is_array($request->property_image))
      {
        foreach ($request->property_image as $key => $property_image)
        {
          $images=new PropertyImage;
          $file = $property_image;
          $path= 'images';
          $document = $file->getClientOriginalName();
          $file->move($path, $document);
          $file = '/'.$path.'/'.$document;
          $images->name = $file;
          $images->property_id=$request->property_id;
          $images->save();
        }
         $propertyImage = Property::find($request->property_id);
         $file=$request->property_image[0];
         $path= 'images';
         $document = $file->getClientOriginalName();
         $file = '/'.$path.'/'.$document;
         $propertyImage->property_image = $file;
         $propertyImage->save();

      }
       return redirect ('/manage/property/view');

    }

    public function editproperty($id)
    {
      $property = Property::find($id);
      return view ('admin.property.editproperty',$property);
    }

    public function updateproperty(Request $request)
    {

      $property = Property::find($request->id);
      $property->agent_id = $request->agent_id;
      $property->name = $request->name;
      $property->description = $request->description;
      $property->rent = $request->rent;
      $property->property_type_id = $request->property_type_id;
      $property->address = $request->address;
      $property->postal_code = $request->postal_code;
      $property->lat = $request->lat;
      $property->long = $request->long;
      $property->formattedaddress = $request->formattedaddress;
      if($request->file('property_image'))
       {
         $file = $request->file('property_image');
         $path= 'images';
         $document = $file->getClientOriginalName();
         $file->move($path, $document);
         $file = $path.'/'.$document;
         $property->property_image = $file;
       }
       $property->save();

      return redirect ('/manage/property/view');
    }

    public function deleteproperty($id)
    {
      $property = Property::find($id);
      $property->delete();
      return \Redirect::back();
    }

    public function viewproperty($id)
    {
      $data['property'] = Property::find($id);

      return view ('admin.property.viewproperty',$data);
    }

    private function filterrent($property,$minRent, $maxRent)
    {
      if(empty($minRent) && empty($maxRent)) return $property;

      if(!empty($minRent) && !empty($maxRent))
      {
        $property = $property->whereBetween('rent', [ $minRent, $maxRent ]);

      }
      elseif (!empty($minRent) && empty($maxRent) )
      {
        $property = $property->where('rent','>=',$minRent);
      }
      elseif (empty($minRent) && !empty($maxRent) )
      {
        $property = $property->where('rent','<=',$maxRent );
      }
      return $property;
    }

    private function filteravailablity($property,$availablity)
    {
       if(empty($availablity)) return $property;

       $property = $property->join('property_availablities as PropAvail','PropAvail.property_id','properties.id')
                           ->select('properties.*','PropAvail.availablity_id');

       $property = $property->whereIn('availablity_id',$availablity);

        return $property;
    }

    private function filterservices($property,$services)
    {
      if(empty($services)) return $property;


      $property = $property->join('property_services as PropService','PropService.property_id','properties.id')
                          ->select('properties.*','PropService.service_id');

      $property = $property->whereIn('service_id',$services);
       return $property;
    }

    private function filterfeatures($property,$features)
    {
      if(empty($features)) return $property;

      $property = $property->join('property_features as PropFeature','PropFeature.property_id','properties.id')
                          ->select('properties.*','PropFeature.feature_id');

      $property = $property->whereIn('feature_id',$features);

       return $property;
    }

    private function filterkeyword($property,$address,$keyword)
    {
      if(empty($address) && empty($keyword)) return $property;

      if (!empty($address))
      {

        $addresses1 =explode(',',$address);
        $addresses2 =explode(' ',$address);

        $addresses = array_filter(array_unique(array_merge($addresses1, $addresses2)));

        foreach ($addresses as $key => $address)
        {
          if($key == 0)
          {
            $property = $property->where('address','like','%'.$address.'%');
          }
          else
          {
            $property = $property->orwhere('address','like','%'.$address.'%');
          }
        }
      }
      elseif (!empty($keyword))
      {
        $keywords1 =explode(',',$keyword);
        $keywords2 =explode(' ',$keyword);

        $keywords = array_filter(array_unique(array_merge($keywords1, $keywords2)));


        foreach ($keywords as $key => $keyword)
        {
          if ($key == 0) {
            $property = $property->where('address','like','%'.$keyword.'%')
                                  ->orwhere('name','like','%'.$keyword.'%')
                                  ->orwhere('description','like','%'.$keyword.'%');
          }
          else {
            $property = $property->orwhere('address','like','%'.$keyword.'%')
                                  ->orwhere('name','like','%'.$keyword.'%')
                                  ->orwhere('description','like','%'.$keyword.'%');;
          }
        }
      }
      return $property;
    }

    private function filterbedrooms($property,$bedrooms)
    {
      if(empty($bedrooms)) return $property;

      foreach ($bedrooms as $key => $bedroom)
      {
        if($key == 0)
        {
          $property = $property->where('bedroom_id','like','%'.$bedroom.'%');
        }
        else
        {
          $property = $property->orwhere('bedroom_id','like','%'.$bedroom.'%');
        }
      }

      return $property;
    }

    public function wishlist($id)
    {
       $wishlist=new Wishlist;
       $wishlist->property_id=$id;
       $wishlist->user_id=Auth::user()->id;
       $wishlist->save();
       return back();
    }
    public function removewishlist($property_id)
    {

       Wishlist::where('property_id',$property_id)->delete();

       return back();
    }
}
