<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Property;
use Auth,Session,Redirect;

class UsersController extends Controller
{
    public function index(Request $request)
    {
      if($request->searchUser)
      {
        $data['users'] = User::join('roles','users.role_id','roles.id')
                              ->select('users.*','roles.name as rolename')
                              ->orwhere('username', 'like', '%' . $request->searchUser . '%')
                              ->orwhere('email', 'like', '%' . $request->searchUser . '%')
                              ->orwhere('phone', 'like', '%' . $request->searchUser . '%')
                              ->orwhere('roles.name', 'like', '%' . $request->searchUser . '%')
                              ->get();
        $data['searchUser'] = $request->searchUser;
        return view('admin.users.index',$data);
      }
      $data['users'] = User::all();
      return view('admin.users.index',$data);
    }
    public function viewprofile($id)
    {
      $data['user'] = User::find($id);
      return view('admin.users.viewprofile',$data);
    }

    public function viewallpropertyimages($id,$propertyId)
    {
      $data['property'] = Property::find($propertyId);
      $data['userid'] = $id;
      return view('admin.users.viewallpropertyimages',$data);
    }
    public function delete($id)
    {
      $user = User::find($id);
      $user->delete();
      return Redirect::back();
    }

    public function accessprofile($id)
    {
      $redirecturl ='/';
      $newuser = '';
      if(Auth::user()->role_id == '1'){
        \Session::put('session_admin',Auth::id());
        \Session::put('url_previous',\URL::previous());
        $newuser = Auth::loginUsingId($id);
      }

      elseif(\Session::has('session_admin')){
          $previousid = \Session::get('session_admin');
          $redirecturl = \Session::get('url_previous');
          Auth::user()->offlineStatus($id);
          Auth::logout();
          $user = Auth::loginUsingId($previousid);
          \Session::forget('session_admin');
          \Session::forget('url_previous');
      }

      return Redirect::to($redirecturl);
    }
}
