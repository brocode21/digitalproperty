<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Auth;


class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
         'phone','password','username','role_id','email','profilepic','address','city','district','country','postal_code','status'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function propertyCount()
    {
      return $this->hasMany('App\Property','agent_id','id')->orderby('updated_at','desc');
    }

    public function userRole()
    {
      return $this->hasOne('App\Role','id','role_id');
    }

    public static function onlineStatus()
    {

        $updateStatus = User::find(Auth::id());
        $updateStatus->status = '1';
        $updateStatus->update();

    }

    public function offlineStatus($id)
    {
      Auth::user()->status = '0';
    }
}
