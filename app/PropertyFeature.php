<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PropertyFeature extends Model
{
    //
    public function featurename()
    {
      return $this->hasOne('App\Feature','id','feature_id');      
    }
}
