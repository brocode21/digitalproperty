<?php

use App\Feature;
use App\Service;
use App\Availablity;

class Helpers
{

  public static function featureName($id)
  {
    return Feature::find($id);
  }
  public static function serviceName($id)
  {
    return Service::find($id);
  }
  public static function availablityName($id)
  {
    return Availablity::find($id);
  }
}



 ?>
